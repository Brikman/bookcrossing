package server.codec.decoder;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.ObjectCodec;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;
import org.apache.commons.collections.map.HashedMap;
import server.model.entity.Address;
import server.model.entity.Picture;
import server.net.packet.request.EditAccountRequest;

import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import static server.net.packet.request.EditAccountRequest.Field.*;

public class EditAccountRequestDeserializer extends JsonDeserializer<EditAccountRequest> {

    @Override
    public EditAccountRequest deserialize(JsonParser parser, DeserializationContext context)
            throws IOException {
        ObjectCodec codec = parser.getCodec();
        JsonNode root = codec.readTree(parser);
        long id = root.get("id").longValue();
        byte code = (byte) root.get("code").intValue();
        byte version = (byte) root.get("version").intValue();

        Map<String, Object> fields = new HashedMap();
        List<String> names = EditAccountRequest.Field.listNames();
        Iterator<Map.Entry<String, JsonNode>> iterator = root.get("fields").fields();
        while (iterator.hasNext()) {
            Map.Entry<String, JsonNode> node = iterator.next();
            String field = node.getKey();
            if (field.equals(ADDRESS.getName())) {
                Address address = codec.treeToValue(node.getValue(), Address.class);
                fields.put(ADDRESS.getName(), address);
            } else if (field.equals(PICTURE.getName())) {
                Picture picture = codec.treeToValue(node.getValue(), Picture.class);
                fields.put(PICTURE.getName(), picture);
            } else if (names.contains(field)) {
                fields.put(field, node.getValue().textValue());
            }
        }

        return new EditAccountRequest(fields, id, code, version);
    }
}
