package server.codec.decoder;

import lombok.Data;

@Data
public class Envelope {

    private long id;
    private byte version;
    private byte type;
    private byte code;
    private byte[] payload;

    public void reset() {
        id = version = type = code = -1;
        payload = null;
    }
}
