package server.codec.decoder;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.ReplayingDecoder;
import lombok.extern.log4j.Log4j2;
import server.codec.TimeModule;
import server.net.packet.Packet;
import server.net.packet.PacketRegistry;
import server.net.packet.response.RejectionPacket;

import java.io.StringReader;
import java.nio.charset.StandardCharsets;
import java.util.List;

@Log4j2
public class Decoder extends ReplayingDecoder<Decoder.DecodingState> {

    protected enum DecodingState {
        ID, VERSION, TYPE, CODE, LENGTH, PAYLOAD
    }

    private Envelope envelope = new Envelope();

    private static ObjectMapper mapper = new ObjectMapper();

    static {
        mapper.registerModule(new TimeModule());
    }

    public Decoder() {
        super(DecodingState.values()[0]);
    }

    /*
     * Envelope format:
     *  _____________________________HEADER(15b)________________________   _BODY(..)__
     * /                                                                \ /           \
     * | id(8b) | version(1b) | type(1b) | code(1b) | payload_length(4b) | payload(..) |
     */
    @Override
    protected void decode(ChannelHandlerContext context, ByteBuf buffer, List<Object> out) throws Exception {
        try {
            switch (state()) {
                case ID:
                    envelope.setId(buffer.readLong());
                    nextState();
                case VERSION:
                    envelope.setVersion(buffer.readByte());
                    nextState();
                case TYPE:
                    envelope.setType(buffer.readByte());
                    nextState();
                case CODE:
                    envelope.setCode(buffer.readByte());
                    nextState();
                case LENGTH:
                    int length = buffer.readInt();
                    if (length <= 0)
                        throw new DecodingException("payload length must be positive: " + length);
                    envelope.setPayload(new byte[length]);
                    nextState();
                case PAYLOAD:
                    buffer.readBytes(envelope.getPayload(), 0, envelope.getPayload().length);
                    Packet packet = decode(envelope);
                    out.add(packet);
                    envelope.reset();
                    nextState();
            }
        } catch (Exception e) {
            log.warn(e.getMessage());
            String message = (e instanceof DecodingException) ? "Malformed packet" : "Internal server error";
            context.writeAndFlush(new RejectionPacket(envelope.getId(), envelope.getVersion(), message));
            envelope.reset();
            checkpoint(DecodingState.values()[0]);
        }
    }

    public Packet decode(Envelope envelope) {
        try {
            byte type = envelope.getType();
            byte code = envelope.getCode();
            String json = new String(envelope.getPayload(), StandardCharsets.UTF_8);
            log.info("RECEIVED: " + json);
            Class<? extends Packet> packetClass = PacketRegistry.forType(type).getPacket(code);
            Packet packet = mapper.readValue(new StringReader(json), packetClass);
            log.info("PACKET RESOLVED: " + packet.getClass().getSimpleName());
            return packet;
        } catch (Exception e) {
            throw new DecodingException(e.getMessage());
        }
    }

    private void nextState() {
        DecodingState[] values = DecodingState.values();
        checkpoint(values[(state().ordinal() + 1) % values.length]);
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext context, Throwable cause) throws Exception {
        log.error("Exception caught", cause);
        checkpoint(DecodingState.VERSION);
    }
}
