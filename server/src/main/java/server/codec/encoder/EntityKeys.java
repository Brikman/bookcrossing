package server.codec.encoder;

import server.model.entity.Picture;
import server.net.dto.*;

import java.util.HashMap;
import java.util.Map;

public class EntityKeys {

    private static final Map<String, Class<? extends EntityDto>> keys =
            new HashMap<String, Class<? extends EntityDto>>() {{
                put("user", UserDto.class);
                put("book", BookDto.class);
                put("transfer", TransferDto.class);
                put("picture", Picture.class);
                put("address", AddressDto.class);
            }};

    public static String getKey(EntityDto entity) {
        for (Map.Entry<String, Class<? extends EntityDto>> entry : keys.entrySet()) {
            if (entry.getValue() == entity.getClass()) {
                return entry.getKey();
            }
        }
        return null;
    }

    public static Class<? extends EntityDto> getByKey(String key) {
        Class<? extends EntityDto> entity = keys.get(key);
        if (entity != null)
            return entity;
        return null;
    }
}
