package server.codec.encoder;

import io.netty.buffer.ByteBuf;
import server.net.packet.Packet;

public abstract class PacketEncoder<T extends Packet> {

    /*
     * Envelope format:
     *  _____________________________HEADER(15b)________________________   _BODY(..)__
     * /                                                                \ /           \
     * | id(8b) | version(1b) | type(1b) | code(1b) | payload_length(4b) | payload(..) |
     */
    public final void encode(T packet, ByteBuf buffer) throws EncodingException {
        byte[] payload = encodePayload(packet);

        buffer.writeLong(packet.getId());
        buffer.writeByte(packet.getVersion());
        buffer.writeByte(packet.getType());
        buffer.writeByte(packet.getCode());
        buffer.writeInt(payload.length);
        buffer.writeBytes(payload);
    }

    public abstract byte[] encodePayload(T packet) throws EncodingException;
}