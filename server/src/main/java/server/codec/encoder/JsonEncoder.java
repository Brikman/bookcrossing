package server.codec.encoder;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.log4j.Log4j2;
import server.codec.TimeModule;
import server.net.packet.Packet;

@Log4j2
public class JsonEncoder extends PacketEncoder<Packet> {

    private static ObjectMapper mapper = new ObjectMapper();

    static {
        mapper.registerModule(new TimeModule());
    }

    @Override
    public byte[] encodePayload(Packet packet) throws EncodingException {
        try {
            String json = mapper.writeValueAsString(packet);
            log.info("SEND JSON: " + (json.length() > 500 ? "<...>" : json));
            return json.getBytes();
        } catch (JsonProcessingException e) {
            throw new EncodingException(e);
        }
    }
}