package server.codec.encoder;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import server.net.dto.EntityDto;
import server.net.packet.response.EntityListResponse;

import java.io.IOException;
import java.util.List;

public class EntityListResponseSerializer extends JsonSerializer<EntityListResponse> {

    @Override
    public void serialize(EntityListResponse response, JsonGenerator gen, SerializerProvider serializers)
            throws IOException {
        gen.writeStartObject();
        gen.writeNumberField("id", response.getId());
        gen.writeNumberField("code", response.getCode());
        gen.writeNumberField("version", response.getVersion());

        List<EntityDto> entities = response.getEntities();
        if (!entities.isEmpty()) {
            gen.writeStringField("entityType", EntityKeys.getKey(entities.get(0)));
        } else {
            gen.writeNullField("entityType");
        }
        gen.writeObjectField("entities", entities);
        gen.writeEndObject();
    }
}
