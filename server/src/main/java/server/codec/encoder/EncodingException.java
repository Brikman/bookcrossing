package server.codec.encoder;

public class EncodingException extends RuntimeException {

    public EncodingException() {}

    public EncodingException(String message) {
        super(message);
    }

    public EncodingException(Throwable cause) {
        super(cause);
    }
}
