package server.codec.encoder;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.MessageToByteEncoder;
import lombok.extern.log4j.Log4j2;
import server.net.packet.Packet;
import server.net.packet.response.PictureResponse;

@Log4j2
public class Encoder extends MessageToByteEncoder<Packet> {

    private static JsonEncoder jsonEncoder = new JsonEncoder();
    private static PictureEncoder pictureEncoder = new PictureEncoder();

    @Override
    protected void encode(ChannelHandlerContext context, Packet packet, ByteBuf buffer) throws Exception {
        log.info("SENDING PACKET: " + packet.getClass().getSimpleName());
        if (packet instanceof PictureResponse) {
            pictureEncoder.encode((PictureResponse) packet, buffer);
        } else {
            jsonEncoder.encode(packet, buffer);
        }
        context.flush();
    }
}
