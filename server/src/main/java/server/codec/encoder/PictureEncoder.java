package server.codec.encoder;

import lombok.extern.log4j.Log4j2;
import server.net.packet.response.PictureResponse;

import java.nio.ByteBuffer;

@Log4j2
public class PictureEncoder extends PacketEncoder<PictureResponse> {

    /*
     *  PictureResponse :
     *      long   id       [8] +
     *      byte   code     [1] +
     *      byte   version  [1] +
     *      int    length   [4] += 14 (bytes)
     *      byte[] picture  [x] = 14 + x (bytes)
     */
    @Override
    public byte[] encodePayload(PictureResponse packet) {
        byte[] picture = packet.getPicture().getBytes();
        log.info("SEND PICTURE: " + picture.length + " bytes");

        ByteBuffer buffer = ByteBuffer.allocate(14 + picture.length);
        buffer.putLong(packet.getId());
        buffer.put(packet.getCode());
        buffer.put(packet.getVersion());
        buffer.putInt(picture.length);
        buffer.put(picture);
        return buffer.array();
    }
}