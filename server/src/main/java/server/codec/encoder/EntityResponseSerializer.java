package server.codec.encoder;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import server.net.packet.response.EntityResponse;

import java.io.IOException;

public class EntityResponseSerializer extends JsonSerializer<EntityResponse> {
    
    @Override
    public void serialize(EntityResponse response, JsonGenerator gen, SerializerProvider serializers)
            throws IOException {
        gen.writeStartObject();
        gen.writeNumberField("id", response.getId());
        gen.writeNumberField("code", response.getCode());
        gen.writeNumberField("version", response.getVersion());
        gen.writeStringField("entityType", EntityKeys.getKey(response.getEntity()));
        gen.writeObjectField("entity", response.getEntity());
        gen.writeEndObject();
    }
}
