package server.model.entity;

import lombok.Data;

import javax.persistence.*;
import java.time.Instant;

@Data
@Entity
@Table(name = "tickets")
public class Ticket implements ModelEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "owner_user_id")
    private User owner;

    @Column(name = "earned_at")
    private Instant earned;

    @Column(name = "spent_at")
    private Instant spent;

    public Ticket() {}
}
