package server.model.entity;

import lombok.Data;

import javax.persistence.*;
import java.time.Instant;

@Data
@Entity
@Table(name = "transfers")
public class Transfer implements ModelEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "sender_id")
    private User sender;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "recipient_id")
    private User recipient;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "book_id")
    private Book book;

    @Column(name = "sent_date_time")
    private Instant sent;

    private String message;

    @Column(name = "track_number")
    private String trackNumber;

    @Enumerated(EnumType.STRING)
    private Status status;

    public Transfer() {}

    public Transfer(User sender, User recipient, Status status) {
        this.sender = sender;
        this.recipient = recipient;
        this.status = status;
    }

    public enum Status {
        PENDING,
        PROCESSING,
        CONFIRMED
    }
}
