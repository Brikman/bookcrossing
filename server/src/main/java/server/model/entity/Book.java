package server.model.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NonNull;
import org.hibernate.annotations.Formula;

import javax.persistence.*;

@EqualsAndHashCode(exclude = "owner")
@Data
@Entity
@Table(name = "books")
public class Book implements ModelEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(nullable = false)
    private String title;

    private String author;

    private String description;

    @OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "picture_id")
    private Picture picture;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "owner_user_id")
    private User owner;

    @Formula("(SELECT IFNULL(AVG(r.rating), 0) FROM reviews r WHERE r.book_id = id)")
    private Float rating;

    public Book() {}

    public Book(String title, String description, String author) {
        this.title = title;
        this.description = description;
        this.author = author;
    }

    public void setOwner(User user) {
        if (owner == null || !owner.equals(user)) {
            owner = user;
            user.addBook(this);
        }
    }

    public void deleteOwner(@NonNull User user) {
        if (owner != null && owner.equals(user)) {
            owner = null;
            user.deleteBook(this);
        }
    }
}