package server.model.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NonNull;

import javax.persistence.*;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

@EqualsAndHashCode(exclude = "books")
@Data
@Entity
@Table(name = "users")
public class User implements ModelEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(nullable = false)
    private String name;

    @Column(nullable = false)
    private String email;

    @Column(nullable = false)
    private String password;

    private String phone;

    @OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "address_id")
    private Address address;

    private String about;

    @OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "picture_id")
    private Picture picture;

    @OneToMany(mappedBy = "owner", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<Book> books = new ArrayList<>();

    @OneToMany(mappedBy = "owner", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<Ticket> tickets = new ArrayList<>();

    public User() {}

    public void addBook(@NonNull Book book) {
        if (!books.contains(book)) {
            books.add(book);
            book.setOwner(this);
        }
    }

    public void deleteBook(@NonNull Book book) {
        if (books.remove(book)) {
            book.deleteOwner(this);
        }
    }

    public void setBooks(List<Book> books) {
        this.books.forEach(this::deleteBook);
        if (books != null) {
            books.forEach(this::addBook);
        }
    }

    public void addTicket(@NonNull Ticket ticket) {
        ticket.setOwner(this);
        ticket.setEarned(Instant.now());
        tickets.add(ticket);
    }

    public void deleteTicket(@NonNull Ticket ticket) {
        ticket.setOwner(null);
        tickets.remove(ticket);
    }
}
