package server.model.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import server.net.dto.EntityDto;

import javax.persistence.*;

@Data
@Entity
@Table(name = "pictures")
@JsonIgnoreProperties(ignoreUnknown = true)
public class Picture implements ModelEntity, EntityDto {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(nullable = false)
    private byte[] bytes;

    public Picture() {}

}
