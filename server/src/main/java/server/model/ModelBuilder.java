package server.model;

import org.apache.commons.lang3.StringUtils;
import server.model.entity.*;
import server.net.dto.*;

import java.util.StringJoiner;

public class ModelBuilder {

    public static Book of(BookDto dto) {
        return update(new Book(), dto);
    }

    public static User of(UserDto dto) {
        return update(new User(), dto);
    }

    public static Address of(AddressDto dto) {
        return update(new Address(), dto);
    }

    public static Review of(ReviewDto dto) {
        return update(new Review(), dto);
    }

    public static User update(User user, UserDto dto) {
        if (dto == null)
            return null;
        user.setName(dto.getName());
        user.setPhone(dto.getPhone());
        user.setAddress(update(user.getAddress(), dto.getAddress()));
        user.setAbout(dto.getAbout());
        user.setPicture(dto.getPicture());
        return user;
    }

    public static Book update(Book book, BookDto dto) {
        if (dto == null)
            return null;
        book.setTitle(dto.getTitle());
        book.setDescription(dto.getDescription());
        book.setPicture(dto.getPicture());

        StringJoiner joiner = new StringJoiner(", ");
        for (String author : dto.getAuthor().split(",")) {
            if (StringUtils.isNotBlank(author)) {
                joiner.add(author.trim().replaceAll(" +", " "));
            }
        }
        book.setAuthor(joiner.toString());
        return book;
    }

    public static Address update(Address address, AddressDto dto) {
        if (dto == null)
            return null;
        address.setCountryCode(dto.getCountryCode());
        address.setCountryName(dto.getCountryName());
        address.setCityCode(dto.getCityCode());
        address.setCityName(dto.getCityName());
        address.setStreet(dto.getStreet());
        address.setHouse(dto.getHouse());
        address.setApartment(dto.getApartment());
        address.setPostalCode(dto.getPostalCode());
        return address;
    }

    public static Review update(Review review, ReviewDto dto) {
        if (dto == null)
            return null;
        review.setComment(dto.getComment());
        review.setRating(dto.getRating());
        return review;
    }
}
