package server.model;

import server.model.entity.Book;
import server.model.entity.ModelEntity;
import server.model.entity.Review;
import server.model.entity.User;

import java.util.HashMap;
import java.util.Map;

/**
 * For EntityTask and PictureTask
 */
public class EntityRegistry {

    private static final HashMap<String,Class<? extends ModelEntity>> registry
            = new HashMap<String, Class<? extends ModelEntity>>() {{
        put("user",   User.class);
        put("book",   Book.class);
    }};

    public static String getKey(ModelEntity entity) {
        for (Map.Entry<String, Class<? extends ModelEntity>> entry : registry.entrySet()) {
            if (entry.getValue() == entity.getClass()) {
                return entry.getKey();
            }
        }
        return null;
    }

    public static Class<? extends ModelEntity> getEntity(String key) {
        return registry.get(key);
    }
}
