package server.dao;

import server.model.entity.Ticket;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import java.util.List;

public class TicketDao extends GenericDao<Ticket, Long> {

    public TicketDao(EntityManager manager) {
        super(Ticket.class, manager);
    }

    public List<Ticket> findLatest(int limit, long excludeId) {
        String qr = "SELECT t FROM Ticket t WHERE t.owner.id != :excludeId " +
                    "AND t.owner.address IS NOT NULL ORDER BY t.earned ASC";
        TypedQuery<Ticket> query = manager.createQuery(qr, Ticket.class);
        query.setParameter("excludeId", excludeId);
        query.setMaxResults(limit);
        return query.getResultList();
    }

    public List<Ticket> findRecent(int limit, long excludeId) {
        String qr = "SELECT t FROM Ticket t WHERE t.owner.id != :excludeId " +
                    "AND t.owner.address IS NOT NULL ORDER BY t.earned DESC";
        TypedQuery<Ticket> query = manager.createQuery(qr, Ticket.class);
        query.setParameter("excludeId", excludeId);
        query.setMaxResults(limit);
        return query.getResultList();
    }
}
