package server.dao;

import server.model.entity.Review;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;
import java.util.List;

public class ReviewDao extends GenericDao<Review, Long> {

    public ReviewDao(EntityManager manager) {
        super(Review.class, manager);
    }

    public Review findByUserAndBook(long userId, long bookId) {
        try {
            String qr = "SELECT r FROM Review r WHERE r.user.id = :userId AND r.book.id = :bookId";
            TypedQuery<Review> query = manager.createQuery(qr, Review.class);
            query.setParameter("userId", userId);
            query.setParameter("bookId", bookId);
            return query.getSingleResult();
        } catch (NoResultException e) {
            return null;
        }
    }

    public List<Review> findByBookId(long bookId) {
        String qr = "SELECT r FROM Review r WHERE r.book.id = :bookId";
        TypedQuery<Review> query = manager.createQuery(qr, Review.class);
        query.setParameter("bookId", bookId);
        return query.getResultList();
    }
}
