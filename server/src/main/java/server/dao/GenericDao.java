package server.dao;

import server.model.entity.ModelEntity;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import java.io.Serializable;
import java.util.List;

public abstract class GenericDao<V extends ModelEntity, K extends Serializable> implements IDao<V, K> {

    protected EntityManager manager;
    private Class<V> type;

    public GenericDao(Class<V> type, EntityManager manager) {
        this.type = type;
        this.manager = manager;
    }

    public void clear() {
        manager.clear();
    }

    @Override
    public V find(K id) {
        return manager.find(type, id);
    }

    @Override
    public List<V> findAll() {
        String qlString = "SELECT t FROM " + type.getName() + " t";
        TypedQuery<V> query = manager.createQuery(qlString, type);
        return query.getResultList();
    }

    @Override
    public V save(V t) {
        manager.persist(t);
        manager.flush();
        return t;
    }

    @Override
    public List<V> saveAll(List<V> entities) {
        entities.forEach(e -> manager.persist(e));
        manager.flush();
        return entities;
    }

    @Override
    public V delete(V t) {
        manager.remove(t);
        manager.flush();
        return t;
    }

    @Override
    public V deleteById(K id) {
        V entity = find(id);
        delete(entity);
        return entity;
    }

    @Override
    public List<V> deleteAll(List<V> entities) {
        entities.forEach(e -> manager.remove(e));
        manager.flush();
        return entities;
    }
}
