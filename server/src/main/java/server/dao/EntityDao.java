package server.dao;

import server.model.entity.ModelEntity;

import javax.persistence.EntityManager;

public class EntityDao extends GenericDao<ModelEntity, Long> {

    public EntityDao(EntityManager manager) {
        super(ModelEntity.class, manager);
    }

    public <E extends ModelEntity> E find(Class<E> entity, Long id) {
        return manager.find(entity, id);
    }
}
