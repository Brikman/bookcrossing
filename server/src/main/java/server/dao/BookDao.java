package server.dao;

import server.model.entity.Book;
import server.model.entity.Transfer;
import server.util.StringUtils;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import java.util.List;

public class BookDao extends GenericDao<Book, Long> {

    public BookDao(EntityManager manager) {
        super(Book.class, manager);
    }

    public List<Book> findByTitle(String title) {
        title = StringUtils.fit(title).toUpperCase();
        String qr = "SELECT b FROM Book b WHERE UPPER(b.title) = :title";
        TypedQuery<Book> query = manager.createQuery(qr, Book.class);
        query.setParameter("title", title);
        return query.getResultList();
    }

    public List<Book> findByUserId(long userId) {
        String qr = "SELECT b FROM Book b WHERE b.owner.id = :userId";
        TypedQuery<Book> query = manager.createQuery(qr, Book.class);
        query.setParameter("userId", userId);
        return query.getResultList();
    }

    public List<Book> findHistoryByUserId(long userId) {
        String qr =
                "SELECT DISTINCT t.book FROM Transfer t " +
                "WHERE t.recipient.id = :userId AND t.status = :status " +
                "OR t.sender.id = :userId";
        TypedQuery<Book> query = manager.createQuery(qr, Book.class);
        query.setParameter("userId", userId);
        query.setParameter("status", Transfer.Status.CONFIRMED);
        return query.getResultList();
    }
}
