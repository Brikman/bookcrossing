package server.dao;

import server.model.entity.Transfer;
import server.model.entity.User;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;
import java.util.List;
import java.util.stream.Collectors;

public class UserDao extends GenericDao<User, Long> {

    public UserDao(EntityManager manager) {
        super(User.class, manager);
    }

    public User findByEmail(String email) {
        try {
            String qr = "SELECT u FROM User u WHERE u.email = :email";
            TypedQuery<User> query = manager.createQuery(qr, User.class);
            query.setParameter("email", email.trim());
            return query.getSingleResult();
        } catch (NoResultException e) {
            return null;
        }
    }

    public List<User> findByBook(long bookId) {
        String qr = "SELECT t FROM Transfer t WHERE t.book.id = :bookId ORDER BY t.sent ASC";
        TypedQuery<Transfer> query = manager.createQuery(qr, Transfer.class);
        query.setParameter("bookId", bookId);
        List<Transfer> transfers = query.getResultList();
        List<User> result = transfers.stream().map(Transfer::getSender).collect(Collectors.toList());
        if (!transfers.isEmpty()) {
            result.add(transfers.get(transfers.size() - 1).getRecipient());
        }
        return result;
    }
}
