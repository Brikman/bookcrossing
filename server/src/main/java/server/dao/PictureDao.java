package server.dao;

import server.model.entity.ModelEntity;
import server.model.entity.Picture;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;

public class PictureDao extends GenericDao<Picture, Long> {

    public PictureDao(EntityManager manager) {
        super(Picture.class, manager);
    }

    public Picture findByEntity(Class<? extends ModelEntity> entity, long entityId) {
        try {
            String entityName = entity.getSimpleName();
            String qr = "SELECT picture AS p FROM " + entityName + " e WHERE e.id = :entityId";
            TypedQuery<Picture> query = manager.createQuery(qr, Picture.class);
            query.setParameter("entityId", entityId);
            return query.getSingleResult();
        } catch (NoResultException e) {
            return null;
        }
    }
}
