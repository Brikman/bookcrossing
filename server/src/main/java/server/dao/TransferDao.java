package server.dao;

import server.model.entity.Transfer;
import server.model.entity.User;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import java.util.List;

public class TransferDao extends GenericDao<Transfer, Long> {

    public TransferDao(EntityManager manager) {
        super(Transfer.class, manager);
    }

    public List<Transfer> findBySenderId(long senderId) {
        return findBySenderId(senderId, null);
    }

    public List<Transfer> findBySenderId(long senderId, Transfer.Status status) {
        String qr = "SELECT t FROM Transfer t WHERE t.sender.id = :senderId";
        if (status != null)
            qr += " AND t.status = :status";
        TypedQuery<Transfer> query = manager.createQuery(qr, Transfer.class);
        query.setParameter("senderId", senderId);
        if (status != null)
            query.setParameter("status", status);
        return query.getResultList();
    }

    public List<Transfer> findByRecipientId(long recipientId) {
        return findByRecipientId(recipientId, null);
    }

    public List<Transfer> findByRecipientId(long recipientId, Transfer.Status status) {
        String qr = "SELECT t FROM Transfer t WHERE t.recipient.id = :recipientId";
        if (status != null)
            qr += " AND t.status = :status";
        TypedQuery<Transfer> query = manager.createQuery(qr, Transfer.class);
        query.setParameter("recipientId", recipientId);
        if (status != null)
            query.setParameter("status", status);
        return query.getResultList();
    }

    public List<Transfer> findByUsers(long senderId, long recipientId) {
        return findByUsers(senderId, recipientId, null);
    }

    public List<Transfer> findByUsers(long senderId, long recipientId, Transfer.Status status) {
        String qr = "SELECT t FROM Transfer t WHERE t.sender.id = :senderId AND t.recipient.id = :recipientId";
        if (status != null)
            qr += " AND t.status = :status";
        TypedQuery<Transfer> query = manager.createQuery(qr, Transfer.class);
        query.setParameter("senderId",    senderId);
        query.setParameter("recipientId", recipientId);
        if (status != null)
            query.setParameter("status", status);
        return query.getResultList();
    }
}
