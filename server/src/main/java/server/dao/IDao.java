package server.dao;

import server.model.entity.ModelEntity;

import java.io.Serializable;
import java.util.List;

public interface IDao<T extends ModelEntity, K extends Serializable> {

    T       find(K id);
    List<T> findAll();
    T       save (T entity);
    List<T> saveAll(List<T> entities);
    T       delete(T entity);
    T       deleteById(K id);
    List<T> deleteAll(List<T> entities);
}
