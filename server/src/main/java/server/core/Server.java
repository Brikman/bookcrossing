package server.core;

import com.google.common.base.Stopwatch;
import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelOption;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import lombok.extern.log4j.Log4j2;
import server.codec.decoder.Decoder;
import server.codec.encoder.Encoder;
import server.core.service.TaskManager;

import java.net.InetSocketAddress;
import java.util.Locale;
import java.util.concurrent.*;

@Log4j2
public class Server extends Thread {

    private InetSocketAddress address;

    private ChannelFuture serverFuture;
    private NioEventLoopGroup bossGroup;
    private NioEventLoopGroup workerGroup;

    private ThreadPoolExecutor authExecutor;
    private ThreadPoolExecutor serverExecutor;
    private TaskManager taskManager;

    public Server(InetSocketAddress address) {
        this.address = address;
    }

    @Override
    public void run() {
        Stopwatch stopwatch = Stopwatch.createStarted();

        bossGroup   = new NioEventLoopGroup();
        workerGroup = new NioEventLoopGroup();

        authExecutor   = createExecutor(1, 1000);
        serverExecutor = createExecutor(20, 3000);
        taskManager    = new TaskManager();

        try {
            ServerBootstrap bootstrap = new ServerBootstrap();
            bootstrap.group(bossGroup, workerGroup)
                    .channel(NioServerSocketChannel.class)
                    .childHandler(new ChannelInitializer<SocketChannel>() {
                        @Override
                        protected void initChannel(SocketChannel channel) throws Exception {
                            channel.pipeline()
                                    .addLast(new Encoder())
                                    .addLast(new Decoder())
                                    .addLast("AuthHandler",   new AuthHandler(authExecutor, taskManager))
                                    .addLast("ServerHandler", new ServerHandler(serverExecutor, taskManager));
                        }
                    })
                    .option(ChannelOption.SO_BACKLOG, 128)
                    .childOption(ChannelOption.SO_KEEPALIVE, true);

            serverFuture = bootstrap.bind(address).sync();
            stopwatch.stop();
            long elapsed = stopwatch.elapsed(TimeUnit.MILLISECONDS);
            log.error(String.format(Locale.US, "Server started on port :%d in %.3fs",
                    address.getPort(), elapsed / 1000.0));
        } catch (InterruptedException e) {
            log.error(e);
            workerGroup.shutdownGracefully();
            bossGroup.shutdownGracefully();
        }
    }

    public void shutdown() {
        if (serverFuture == null)
            return;
        try {
            serverFuture.channel().closeFuture().sync();
        } catch (InterruptedException e) {
            log.error(e);
        } finally {
            workerGroup.shutdownGracefully();
            bossGroup.shutdownGracefully();
            log.info("Server terminated");
        }
    }

    private ThreadPoolExecutor createExecutor(int threads, int queueCapacity) {
        return new ThreadPoolExecutor(threads, threads, 0L, TimeUnit.MILLISECONDS,
                new LinkedBlockingQueue<>(queueCapacity));
    }
}
