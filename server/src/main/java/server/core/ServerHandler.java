package server.core;

import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import lombok.extern.log4j.Log4j2;
import server.core.service.TaskManager;
import server.core.service.ex.ServiceException;
import server.core.service.task.ServerTask;
import server.net.packet.Packet;
import server.net.packet.request.Request;
import server.net.packet.response.RejectionPacket;

import java.util.concurrent.ThreadPoolExecutor;

@Log4j2
public class ServerHandler extends ChannelInboundHandlerAdapter {

    private final ThreadPoolExecutor executor;
    private final TaskManager taskManager;

    public ServerHandler(ThreadPoolExecutor executor, TaskManager taskManager) {
        this.executor = executor;
        this.taskManager = taskManager;
    }

    @Override
    public void channelRead(ChannelHandlerContext context, Object msg) throws Exception {
        if (msg instanceof Packet) {
            log.info("Packet received: " + msg.getClass().getSimpleName());
            try {
                ServerTask task = taskManager.taskFor((Packet) msg, context);
                if (executor.getQueue().remainingCapacity() == 0) {
                    task.reject("Server is overloaded");
                } else {
                    executor.execute(task);
                }
            } catch (ServiceException e) {
                log.warn("Service exception: " + e.getMessage());
                if (msg instanceof Request) {
                    context.writeAndFlush(new RejectionPacket(e.getMessage(), (Request) msg));
                }
            }
        } else {
            log.warn("Incorrect message: " + msg);
        }
    }

    @Override
    public void channelUnregistered(ChannelHandlerContext context) throws Exception {
        log.info("Channel unregistered: " + context.channel().remoteAddress());
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext context, Throwable cause) {
        log.error("FATAL: uncaught exception in ServerHandler", cause);
        context.close();
    }
}
