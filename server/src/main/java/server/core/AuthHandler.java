package server.core;

import io.netty.channel.*;
import lombok.extern.log4j.Log4j2;
import server.core.service.TaskManager;
import server.core.service.task.ServerTask;
import server.net.packet.request.AuthRequest;
import server.net.packet.request.RegistrationRequest;
import server.net.packet.request.Request;
import server.net.packet.response.RejectionPacket;

import java.util.concurrent.ThreadPoolExecutor;

@Log4j2
public class AuthHandler extends ChannelInboundHandlerAdapter {

    private final ThreadPoolExecutor executor;
    private final TaskManager taskManager;

    public AuthHandler(ThreadPoolExecutor executor, TaskManager taskManager) {
        this.executor = executor;
        this.taskManager = taskManager;
    }

    @Override
    public void channelRegistered(ChannelHandlerContext context) {
        try {
            log.info("Channel registered: " + context.channel().remoteAddress());
        } catch (ChannelException e) {
            log.error(e);
        }
    }

    @Override
    public void channelRead(ChannelHandlerContext context, Object msg) throws Exception {
        if (msg instanceof Request) {
            Request request = (Request) msg;
            if (request instanceof AuthRequest || request instanceof RegistrationRequest) {
                ServerTask task = taskManager.taskFor(request, context);
                if (executor.getQueue().remainingCapacity() == 0) {
                    task.reject("Server is overloaded");
                } else {
                    executor.execute(task);
                }
            } else {
                context.writeAndFlush(new RejectionPacket("unauthorized", request));
            }
        }
    }

    @Override
    public void channelUnregistered(ChannelHandlerContext context) {
        log.info("Channel unregistered:  " + context.channel().remoteAddress());
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext context, Throwable cause) {
        log.error("FATAL: uncaught exception in AuthHandler: " + cause);
        context.close();
    }
}
