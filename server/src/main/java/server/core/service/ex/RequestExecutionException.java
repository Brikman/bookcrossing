package server.core.service.ex;

import server.util.ErrorWrapper;

/**
 * Used if some errors occurred while processing task for corresponding request.
 * Used because of incorrect or invalid request content.
 */
public class RequestExecutionException extends RuntimeException {

    private ErrorWrapper wrapper;

    public RequestExecutionException(ErrorWrapper wrapper) {
        this.wrapper = wrapper;
    }

    public RequestExecutionException(String logReply) {
        wrapper = new ErrorWrapper(logReply);
    }

    public RequestExecutionException(String log, String reply) {
        wrapper = new ErrorWrapper(log, reply);
    }

    public RequestExecutionException(String log, String reply, Exception cause) {
        wrapper = new ErrorWrapper(log, reply, cause);
    }

    public ErrorWrapper getWrapper() {
        return wrapper;
    }
}
