package server.core.service.ex;

/**
 * Used when serving request and/or assigning corresponding task.
 * Used mostly because of technical reasons, not because of request content.
 */
public class ServiceException extends Exception {

    public ServiceException(String message) {
        super(message);
    }
}
