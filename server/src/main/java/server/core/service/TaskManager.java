package server.core.service;

import io.netty.channel.ChannelHandlerContext;
import lombok.extern.log4j.Log4j2;
import server.core.service.ex.ServiceException;
import server.core.service.task.*;
import server.net.packet.Packet;
import server.net.packet.request.*;
import server.util.PersistenceUnit;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.util.HashMap;

@Log4j2
public class TaskManager {

    private static final HashMap<Class<? extends Packet>, Class<? extends ServerTask>> REGISTRY
                   = new HashMap<Class<? extends Packet>, Class<? extends ServerTask>>() {{
        put(EchoRequest.class,             EchoTask.class);
        put(AuthRequest.class,             AuthTask.class);
        put(RegistrationRequest.class,     RegistrationTask.class);
        put(EditAccountRequest.class,      EditAccountTask.class);
        put(EntityRequest.class,           EntityTask.class);
        put(BookListRequest.class,         BookListTask.class);
        put(SendBookRequest.class,         SendBookTask.class);
        put(TransferListRequest.class,     TransferListTask.class);
        put(ConfirmReceptionRequest.class, ConfirmReceptionTask.class);
        put(RecipientListRequest.class,    RecipientListTask.class);
        put(PictureRequest.class,          PictureTask.class);
        put(EditTransferRequest.class,     EditTransferTask.class);
        put(EditBookRequest.class,         EditBookTask.class);
        put(BookOwnersRequest.class,       BookOwnersTask.class);
        put(LeaveReviewRequest.class,      ReviewTask.class);
        put(ReviewListRequest.class,       ReviewListTask.class);
    }};

    private final EntityManagerFactory factory;

    public TaskManager() {
        factory = Persistence.createEntityManagerFactory(PersistenceUnit.CURRENT);
    }

    public ServerTask taskFor(Packet packet, ChannelHandlerContext context) throws ServiceException {
        try {
            ServerTask task = REGISTRY.get(packet.getClass()).newInstance();
            task.init(packet, context, this);
            return task;
        } catch (ReflectiveOperationException e) {
            log.error("Cannot instantiate task", e);
        } catch (NullPointerException e) {
            log.error("Cannot find task for given request: " + packet.getClass().getSimpleName());
        }
        throw new ServiceException("cannot process request: unknown request");
    }

    public EntityManager getConnection() {
        return factory.createEntityManager();
    }

    public void shutdown() {
        factory.close();
    }
}
