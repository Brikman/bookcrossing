package server.core.service.task;

import lombok.extern.log4j.Log4j2;
import server.cache.UserCache;
import server.dao.Dao;
import server.dao.UserDao;
import server.model.entity.Address;
import server.model.entity.Picture;
import server.model.entity.User;
import server.net.packet.request.EditAccountRequest;
import server.net.packet.response.StatusResponse;

import static server.net.packet.request.EditAccountRequest.Field.*;

@Log4j2
public class EditAccountTask extends ServerTask<EditAccountRequest> {

    @Dao
    private UserDao userDao;

    @Override
    public void process() {
        UserCache cache = UserCache.fromContext(context);

        try {
            getTransaction().begin();

            User user = userDao.find(cache.getId());
            if (request.hasChanged(NAME)) 
                user.setName((String) request.getField(NAME));
            if (request.hasChanged(EMAIL))
                user.setEmail((String) request.getField(EMAIL));
            if (request.hasChanged(PHONE))
                user.setPhone((String) request.getField(PHONE));
            if (request.hasChanged(ADDRESS))
                user.setAddress((Address) request.getField(ADDRESS));
            if (request.hasChanged(ABOUT))
                user.setAbout((String) request.getField(ABOUT));
            if (request.hasChanged(PICTURE))
                user.setPicture((Picture) request.getField(PICTURE));
            
            userDao.save(user);
            cache.update(user);

            getTransaction().commit();

            log.info("Success edit account for: " + user.getEmail());
            reply(new StatusResponse(true, "account edit success", request));
        } catch (Exception e) {
            log.warn("Edit account failed", e);
            getTransaction().rollback();
            reply(new StatusResponse(false, "account edit failed", request));
        }
    }
}
