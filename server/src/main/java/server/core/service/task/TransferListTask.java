package server.core.service.task;

import lombok.extern.log4j.Log4j2;
import server.cache.UserCache;
import server.dao.Dao;
import server.dao.TransferDao;
import server.model.entity.Transfer;
import server.net.packet.request.TransferListRequest;

import java.util.List;

import static server.model.entity.Transfer.Status;
import static server.net.packet.request.TransferListRequest.TransferListOption.INCOMING;

@Log4j2
public class TransferListTask extends ServerTask<TransferListRequest> {

    @Dao
    private TransferDao dao;

    @Override
    public void process() {
        try {
            getTransaction().begin();
            List<Transfer> transfers;
            long id = UserCache.fromContext(context).getId();
            if (request.getOption() == INCOMING) {
                transfers = dao.findByRecipientId(id, Status.PROCESSING);
            } else {
                transfers = dao.findBySenderId(id, Status.PROCESSING);
            }

            getTransaction().commit();
            reply(transfers);
            log.info("Transfer list sent on TransferListOption." + request.getOption());
        } catch (Exception e) {
            log.error("Transfer list error", e);
            getTransaction().rollback();
            reject("internal server error");
        }
    }
}
