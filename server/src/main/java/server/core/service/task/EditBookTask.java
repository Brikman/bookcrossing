package server.core.service.task;

import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.StringUtils;
import server.cache.UserCache;
import server.core.service.ex.RequestExecutionException;
import server.dao.BookDao;
import server.dao.Dao;
import server.model.ModelBuilder;
import server.model.entity.Book;
import server.net.dto.BookDto;
import server.net.packet.request.EditBookRequest;
import server.net.packet.response.StatusResponse;

@Log4j2
public class EditBookTask extends ServerTask<EditBookRequest> {

    @Dao
    private BookDao bookDao;

    @Override
    public void process() {
        UserCache cache = UserCache.fromContext(context);
        BookDto dto = request.getBook();

        try {
            getTransaction().begin();

            Book book = bookDao.find(dto.getId());
            if (book == null) {
                throw new RequestExecutionException("No such book by id:" + dto.getId());
            }
            if (book.getOwner().getId() != cache.getId()) {
                throw new RequestExecutionException("Not allowed to edit this book");
            }

            ModelBuilder.update(book, dto);
            if (StringUtils.isBlank(book.getTitle())) {
                throw new RequestExecutionException("Empty title is not allowed");
            }
            if (StringUtils.isBlank(book.getAuthor())) {
                throw new RequestExecutionException("Empty author is not allowed");
            }
            bookDao.save(book);

            reply(StatusResponse.success("Success edit book", request));
            getTransaction().commit();
        } catch (RequestExecutionException e) {
            getTransaction().rollback();
            log.warn(e.getWrapper().getLog());
            reject(e.getWrapper().getReply());
        } catch (Exception e) {
            getTransaction().rollback();
            log.warn(e);
            reject("Internal server error");
        }
    }
}
