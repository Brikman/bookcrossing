package server.core.service.task;

import lombok.extern.log4j.Log4j2;
import server.dao.Dao;
import server.dao.UserDao;
import server.net.packet.request.BookOwnersRequest;

@Log4j2
public class BookOwnersTask extends ServerTask<BookOwnersRequest> {

    @Dao
    private UserDao userDao;

    @Override
    public void process() {
        try {
            getTransaction().begin();
            reply(userDao.findByBook(request.getBookId()));
            getTransaction().commit();
        } catch (Exception e) {
            getTransaction().rollback();
            log.error(e);
            reject("Internal server error");
        }
    }
}
