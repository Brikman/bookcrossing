package server.core.service.task;

import lombok.extern.log4j.Log4j2;
import server.dao.Dao;
import server.dao.PictureDao;
import server.model.EntityRegistry;
import server.model.entity.ModelEntity;
import server.model.entity.Picture;
import server.net.packet.request.PictureRequest;
import server.net.packet.response.PictureResponse;

@Log4j2
public class PictureTask extends ServerTask<PictureRequest> {

    @Dao
    private PictureDao pictureDao;

    @Override
    public void process() {
        String entityName = request.getEntityName();
        long entityId = request.getEntityId();

        Class<? extends ModelEntity> entityClass = EntityRegistry.getEntity(entityName);
        if (entityClass == null) {
            log.error("Unknown entity key: " + entityName);
            reject("Unknown entity key: " + entityName);
            return;
        }

        try {
            getTransaction().begin();
            Picture picture = pictureDao.findByEntity(entityClass, entityId);
            if (picture != null) {
                reply(new PictureResponse(picture, request));
            } else {
                reject("Cannot find picture by given id");
            }
            getTransaction().commit();
        } catch (Exception e) {
            log.error("Error responding picture", e);
            getTransaction().rollback();
            reject("Internal server error");
        }
    }
}
