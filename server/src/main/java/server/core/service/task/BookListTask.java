package server.core.service.task;

import lombok.extern.log4j.Log4j2;
import server.cache.UserCache;
import server.dao.BookDao;
import server.dao.Dao;
import server.model.entity.Book;
import server.net.packet.request.BookListRequest;

import java.util.List;

@Log4j2
public class BookListTask extends ServerTask<BookListRequest> {

    @Dao
    private BookDao bookDao;

    @Override
    public void process() {
        try {
            getTransaction().begin();
            UserCache cache = UserCache.fromContext(context);

            String condition = request.getCondition();
            String value = request.getValue();

            List<Book> books = null;
            switch (condition) {
                case "user":
                    books = bookDao.findByUserId(Long.parseLong(value));
                    break;
                case "title":
                    books = bookDao.findByTitle(value);
                    break;
                case "history":
                    books = bookDao.findHistoryByUserId(cache.getId());
                    break;
                default:
                    throw new NullPointerException();
            }
            getTransaction().commit();
            reply(books);
            log.info("book list sent");
        } catch (NullPointerException e) {
            getTransaction().rollback();
            log.error("Invalid or missing request parameters");
            reject("Invalid or missing request parameters");
        } catch (NumberFormatException e) {
            getTransaction().rollback();
            String message = "Invalid request parameter value: value = " + request.getValue();
            log.warn(message);
            reject(message);
        } catch (Exception e) {
            getTransaction().rollback();
            log.error("Book list error", e);
            reject("internal server error");
        }
    }
}
