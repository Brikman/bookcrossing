package server.core.service.task;

import lombok.extern.log4j.Log4j2;
import org.apache.commons.validator.routines.EmailValidator;
import server.cache.ChannelCache;
import server.dao.Dao;
import server.dao.UserDao;
import server.model.entity.Ticket;
import server.model.entity.User;
import server.net.dto.DtoBuilder;
import server.net.dto.UserDto;
import server.net.packet.request.RegistrationRequest;
import server.util.StringUtils;

import javax.persistence.PersistenceException;

@Log4j2
public class RegistrationTask extends ServerTask<RegistrationRequest> {

    @Dao
    private UserDao userDao;

    @Override
    public void process() {
        if (context.pipeline().get("AuthHandler") == null) {
            reject("already authorized");
            return;
        }

        if (!EmailValidator.getInstance().isValid(request.getEmail())) {
            reject("registration failed: invalid email format");
            log.warn("Registration failed: invalid email format: " + request.getEmail());
            return;
        }

        User user = new User();
        user.setName(request.getName());
        user.setEmail(request.getEmail());
        user.setPassword(StringUtils.hash(request.getPassword()));
        user.addTicket(new Ticket());

        try {
            getTransaction().begin();
            user = userDao.save(user);
            UserDto userDto = DtoBuilder.of(user);
            ChannelCache cache = new ChannelCache(context);
            cache.getUser().update(user);
            getTransaction().commit();

            reply(userDto);
            context.pipeline().remove("AuthHandler");
        } catch (PersistenceException e) {
            log.warn("Registration failed", e);
            getTransaction().rollback();
            reject("registration failed: login is already in use");
        } catch (Exception e) {
            log.error("Registration failed: " + e);
            getTransaction().rollback();
            reject("internal server error");
        }
    }
}
