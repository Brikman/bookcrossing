package server.core.service.task;

import lombok.extern.log4j.Log4j2;
import server.core.service.ex.RequestExecutionException;
import server.dao.Dao;
import server.dao.EntityDao;
import server.model.EntityRegistry;
import server.model.entity.ModelEntity;
import server.net.dto.DtoBuilder;
import server.net.dto.EntityDto;
import server.net.packet.request.EntityRequest;

@Log4j2
public class EntityTask extends ServerTask<EntityRequest> {

    @Dao
    private EntityDao entityDao;

    @Override
    public void process() {
        try {
            getTransaction().begin();
            Class<? extends ModelEntity> entityClass = EntityRegistry.getEntity(request.getEntityName());
            if (entityClass == null) {
                throw new RequestExecutionException("Unknown entity request: " + request.getEntityName());
            }
            long entityId = request.getEntityId();
            ModelEntity entity = entityDao.find(entityClass, entityId);
            if (entity == null) {
                throw new RequestExecutionException("No entity found with id=" + request.getEntityId());
            }
            EntityDto dto = DtoBuilder.of(entity);
            reply(dto);
            getTransaction().commit();
        } catch (RequestExecutionException e) {
            log.error(e.getWrapper().getLog());
            getTransaction().rollback();
            reject(e.getWrapper().getReply());
        } catch (Exception e) {
            log.error("Entity request error", e);
            getTransaction().rollback();
            reject("internal server error");
        }
    }
}
