package server.core.service.task;

import lombok.extern.log4j.Log4j2;
import server.cache.UserCache;
import server.dao.Dao;
import server.dao.TicketDao;
import server.dao.TransferDao;
import server.dao.UserDao;
import server.model.entity.Ticket;
import server.model.entity.Transfer;
import server.model.entity.User;
import server.net.dto.DtoBuilder;
import server.net.dto.UserDto;
import server.net.packet.request.RecipientListRequest;
import server.net.packet.response.RecipientListResponse;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Log4j2
public class RecipientListTask extends ServerTask<RecipientListRequest> {

    @Dao
    private UserDao userDao;
    @Dao
    private TransferDao transferDao;
    @Dao
    private TicketDao ticketDao;

    private static final int PENDING_THRESHOLD = 5;

    @Override
    public void process() {
        try {
            getTransaction().begin();
            User sender = userDao.find(UserCache.fromContext(context).getId());
            List<Transfer> pendings = transferDao.findBySenderId(sender.getId(), Transfer.Status.PENDING);
            int recipientsLimit = PENDING_THRESHOLD - pendings.size();

            List<Ticket> tickets = ticketDao.findLatest(recipientsLimit, sender.getId());
            int availableMore = tickets.size();
            if (request.getOption() == RecipientListRequest.LIST_PENDING) {
                List<UserDto> result = pendings.stream()
                        .map(Transfer::getRecipient)
                        .map(DtoBuilder::of)
                        .collect(Collectors.toList());
                reply(new RecipientListResponse(result, availableMore, request));
            } else if (request.getOption() == RecipientListRequest.LIST_NEW) {
                if (availableMore > 0) {
                    Instant now = Instant.now();
                    List<User> recipients = new ArrayList<>();
                    List<Transfer> transfers = new ArrayList<>();
                    for (Ticket ticket : tickets) {
                        transfers.add(new Transfer(sender, ticket.getOwner(), Transfer.Status.PENDING));
                        recipients.add(ticket.getOwner());
                        ticket.setSpent(now);
                    }
                    transferDao.saveAll(transfers);
                    ticketDao.saveAll(tickets);

                    List<UserDto> result = recipients.stream()
                            .map(DtoBuilder::of)
                            .collect(Collectors.toList());
                    reply(new RecipientListResponse(result, recipientsLimit, request));
                } else {
                    reject("New recipients limit exceeded");
                }
            } else {
                reject("Invalid request option: " + request.getOption());
            }

            getTransaction().commit();
        } catch (Exception e) {
            log.warn("Recipients list error", e);
            getTransaction().rollback();
            reject("Internal server error");
        }
    }
}
