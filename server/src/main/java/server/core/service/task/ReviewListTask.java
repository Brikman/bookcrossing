package server.core.service.task;

import lombok.extern.log4j.Log4j2;
import server.dao.BookDao;
import server.dao.Dao;
import server.dao.ReviewDao;
import server.model.entity.Book;
import server.model.entity.Review;
import server.net.dto.DtoBuilder;
import server.net.dto.ReviewDto;
import server.net.packet.request.ReviewListRequest;
import server.net.packet.response.ReviewListResponse;

import java.util.List;
import java.util.stream.Collectors;

@Log4j2
public class ReviewListTask extends ServerTask<ReviewListRequest> {

    @Dao
    private BookDao bookDao;
    @Dao
    private ReviewDao reviewDao;

    @Override
    public void process() {
        try {
            getTransaction().begin();

            Book book = bookDao.find(request.getBookId());
            if (book == null) {
                reject("No such book found");
            } else {
                List<Review> reviews = reviewDao.findByBookId(request.getBookId());

                List<ReviewDto> result = reviews.stream().map(review -> {
                    ReviewDto dto = DtoBuilder.of(review);
                    dto.setBook(null);
                    return dto;
                }).collect(Collectors.toList());

                reply(new ReviewListResponse(DtoBuilder.of(book), result, request));
            }
            getTransaction().commit();
        } catch (Exception e) {
            getTransaction().rollback();
            log.error(e);
            reject("Internal server error");
        }
    }
}
