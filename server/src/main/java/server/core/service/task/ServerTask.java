package server.core.service.task;

import com.google.common.base.Stopwatch;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelHandlerContext;
import lombok.extern.log4j.Log4j2;
import server.core.service.TaskManager;
import server.dao.Dao;
import server.dao.IDao;
import server.model.entity.ModelEntity;
import server.net.dto.DtoBuilder;
import server.net.dto.EntityDto;
import server.net.packet.Packet;
import server.net.packet.request.Request;
import server.net.packet.response.EntityListResponse;
import server.net.packet.response.EntityResponse;
import server.net.packet.response.RejectionPacket;
import server.net.packet.response.Response;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import java.lang.reflect.Field;
import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.TimeUnit;

@Log4j2
public abstract class ServerTask<T extends Packet> implements Runnable {

    protected ChannelHandlerContext context;
    protected T request;

    private TaskManager parentManager;
    private EntityManager entityManager;

    public void init(T request, ChannelHandlerContext context, TaskManager parentManager) {
        this.context = Objects.requireNonNull(context);
        this.request = Objects.requireNonNull(request);
        this.parentManager = Objects.requireNonNull(parentManager);
    }

    @Override
    public final void run() {
        log.info("EXECUTION STARTED: " + this.getClass().getSimpleName());
        Stopwatch stopwatch = Stopwatch.createUnstarted();
        try {
            preprocess();
            stopwatch.start();
            process();
        } catch (Exception e) {
            onException(e);
        } finally {
            try {
                EntityTransaction transaction = entityManager.getTransaction();
                if (transaction.isActive()) {
                    log.warn("Active unmanaged transaction detected -> rollback");
                    transaction.rollback();
                }
            } catch (Exception e) {
                log.error("Error while autorollback unmanaged transaction", e);
            }
            stopwatch.stop();
            log.info(String.format("EXECUTED: %s, elapsed time: %d ms",
                    this.getClass().getSimpleName(),
                    stopwatch.elapsed(TimeUnit.MILLISECONDS))
            );
        }
    }

    private void preprocess() throws Exception {
        entityManager = parentManager.getConnection();

        for (Field field : this.getClass().getDeclaredFields()) {
            Class<?> type = field.getType();
            if (field.isAnnotationPresent(Dao.class) && IDao.class.isAssignableFrom(type)) {
                IDao dao = (IDao) type.getConstructor(EntityManager.class).newInstance(entityManager);
                field.setAccessible(true);
                field.set(this, dao);
            }
        }
    }

    protected EntityTransaction getTransaction() {
        return entityManager.getTransaction();
    }

    public abstract void process();

    public void onException(Exception e) {
        log.error("Exception caught", e);
        if (request instanceof Request) {
            reject("Internal server error");
        }
    }

    public ChannelFuture reply(Response response) {
        return context.writeAndFlush(response);
    }

    public <E extends ModelEntity> ChannelFuture reply(Collection<E> entities) {
        if (request instanceof Request) {
            List<EntityDto> list = DtoBuilder.listOf(entities);
            EntityListResponse<? extends EntityDto> response = new EntityListResponse<>(list, (Request) request);
            return context.writeAndFlush(response);
        }
        throw new UnsupportedOperationException("non-repliable request: " + request);
    }

    public <E extends EntityDto> ChannelFuture reply(E dto) {
        if (request instanceof Request) {
            EntityResponse<E> response = new EntityResponse<>(dto, (Request) request);
            return context.writeAndFlush(response);
        }
        throw new UnsupportedOperationException("non-repliable request: " + request);
    }

    public ChannelFuture reject(String message) {
        if (request instanceof Request) {
            log.warn("Request " + request.getClass().getSimpleName() + " rejected: " + message);
            return context.writeAndFlush(new RejectionPacket(message, (Request) request));
        }
        throw new UnsupportedOperationException("non-rejectable request: " + request);
    }
}
