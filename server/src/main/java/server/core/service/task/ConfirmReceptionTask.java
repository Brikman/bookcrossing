package server.core.service.task;

import lombok.extern.log4j.Log4j2;
import server.cache.UserCache;
import server.core.service.ex.RequestExecutionException;
import server.dao.Dao;
import server.dao.TransferDao;
import server.dao.UserDao;
import server.model.entity.Ticket;
import server.model.entity.Transfer;
import server.model.entity.Transfer.Status;
import server.model.entity.User;
import server.net.packet.request.ConfirmReceptionRequest;
import server.net.packet.response.StatusResponse;

@Log4j2
public class ConfirmReceptionTask extends ServerTask<ConfirmReceptionRequest> {

    @Dao
    private TransferDao transferDao;
    @Dao
    private UserDao userDao;

    @Override
    public void process() {
        try {
            getTransaction().begin();

            Transfer transfer = transferDao.find(request.getTransferId());
            if (transfer == null) {
                throw new RequestExecutionException(
                        "Try to confirm non-existing transfer by id: " + request.getTransferId(),
                        "illegal request parameters"
                );
            }
            long recipientId = UserCache.fromContext(context).getId();
            if (transfer.getRecipient().getId() != recipientId) {
                throw new RequestExecutionException(
                        "Try to confirm improper transfer id: " + request.getTransferId() + " by user id: " + recipientId,
                        "illegal request parameters"
                );
            }
            transfer.setStatus(Status.CONFIRMED);
            transferDao.save(transfer);

            User sender = transfer.getSender();
            sender.addTicket(new Ticket());
            userDao.save(sender);

            User recipient = transfer.getRecipient();
            recipient.addBook(transfer.getBook());
            userDao.save(recipient);

            getTransaction().commit();
            log.info("Transfer confirmed id: " + request.getTransferId());
            reply(new StatusResponse(true, "transfer successfully confirmed", request));
        } catch (RequestExecutionException e) {
            log.error(e.getWrapper().getLog());
            getTransaction().rollback();
            reject(e.getWrapper().getReply());
        } catch (Exception e) {
            log.error("Confirm reception error", e);
            getTransaction().rollback();
            reject("internal server error");
        }
    }
}
