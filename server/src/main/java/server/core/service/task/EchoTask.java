package server.core.service.task;

import lombok.extern.log4j.Log4j2;
import server.net.packet.request.EchoRequest;
import server.net.packet.response.EchoResponse;

@Log4j2
public class EchoTask extends ServerTask<EchoRequest> {

    private static int count = 0;

    @Override
    public void process() {
//        System.out.println(count++);
        reply(new EchoResponse(request));
    }
}
