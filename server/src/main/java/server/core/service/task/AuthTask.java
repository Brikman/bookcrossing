package server.core.service.task;

import lombok.extern.log4j.Log4j2;
import org.apache.commons.validator.routines.EmailValidator;
import server.cache.ChannelCache;
import server.core.service.ex.RequestExecutionException;
import server.dao.Dao;
import server.dao.UserDao;
import server.model.entity.User;
import server.net.dto.DtoBuilder;
import server.net.dto.UserDto;
import server.net.packet.request.AuthRequest;
import server.util.StringUtils;

@Log4j2
public class AuthTask extends ServerTask<AuthRequest> {

    @Dao
    private UserDao userDao;

    public void process() {
        if (context.pipeline().get("AuthHandler") == null) {
            reject("already authorized");
            return;
        }
        if (!EmailValidator.getInstance().isValid(request.getEmail())) {
            reject("registration failed: invalid email format");
            log.warn("Authentication failed: invalid email format: " + request.getEmail());
            return;
        }
        try {
            getTransaction().begin();
            User user = userDao.findByEmail(request.getEmail());
            if (user == null || !user.getPassword().equals(StringUtils.hash(request.getPassword()))) {
                throw new RequestExecutionException(
                        "Authorization failed: [" + request.getEmail() + "]-[" + request.getPassword() + "]",
                        "incorrect email or password"
                );
            }
            log.info("Authorized: " + user.getId());
            UserDto dto = DtoBuilder.of(user);
            ChannelCache cache = new ChannelCache(context);
            cache.getUser().update(user);
            getTransaction().commit();

            reply(dto);
            context.pipeline().remove("AuthHandler");
        } catch (RequestExecutionException e) {
            log.error(e.getWrapper().getLog());
            getTransaction().rollback();
            reject(e.getWrapper().getReply());
        } catch (Exception e) {
            log.error("Authorization error", e);
            getTransaction().rollback();
            reject("internal server error");
        }
    }
}
