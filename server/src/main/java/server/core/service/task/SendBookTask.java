package server.core.service.task;

import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.StringUtils;
import server.cache.UserCache;
import server.core.service.ex.RequestExecutionException;
import server.dao.BookDao;
import server.dao.Dao;
import server.dao.TransferDao;
import server.dao.UserDao;
import server.model.ModelBuilder;
import server.model.entity.Book;
import server.model.entity.Transfer;
import server.model.entity.User;
import server.net.dto.BookDto;
import server.net.packet.request.SendBookRequest;
import server.net.packet.response.StatusResponse;

import java.time.Instant;
import java.util.List;
import java.util.Optional;

import static server.net.packet.request.SendBookRequest.SendOption.SEND_NEW;

@Log4j2
public class SendBookTask extends ServerTask<SendBookRequest> {

    @Dao
    private UserDao userDao;
    @Dao
    private BookDao bookDao;
    @Dao
    private TransferDao transferDao;

    @Override
    public void process() {
        try {
            getTransaction().begin();
            User sender = userDao.find(UserCache.fromContext(context).getId());
            User recipient = userDao.find(request.getRecipientId());
            if (recipient == null) {
                throw new RequestExecutionException("Incorrect recipient id: " + request.getRecipientId());
            }
            if (recipient.getTickets().size() <= 0) {
                throw new RequestExecutionException(
                        "Not enough tickets: " +
                                "user " + request.getRecipientId() +
                                " have " + recipient.getTickets().size() + " tickets",
                        "Recipient user doesn't have enough tickets"
                );
            }
            Book book;
            BookDto dto = request.getBook();
            if (request.getSendOption() == SEND_NEW) {
                // REGISTER NEW BOOK
                book = ModelBuilder.of(dto);
                if (StringUtils.isBlank(book.getTitle())) {
                    throw new RequestExecutionException("Empty title is not allowed");
                }
                if (StringUtils.isBlank(book.getAuthor())) {
                    throw new RequestExecutionException("Empty author is not allowed");
                }
                bookDao.save(book);
            } else {
                // FORWARD EXISTING BOOK
                Optional<Book> opt = sender.getBooks().stream()
                        .filter(b -> b.getId() == dto.getId())
                        .findFirst();
                if (!opt.isPresent()) {
                    throw new RequestExecutionException(
                            "Forwarding non-existing or improper book id: " + dto.getId(),
                            "try to forward non-existing or improper book"
                    );
                }
                book = opt.get();
                sender.deleteBook(book);
            }

            List<Transfer> transferList =
                    transferDao.findByUsers(sender.getId(), recipient.getId(), Transfer.Status.PENDING);
            if (transferList.isEmpty()) {
                throw new RequestExecutionException(
                        "Attempt of sending without pending transfer: " + sender.getId() + " -> " + recipient.getId(),
                        "Attempt of sending book without pending transfer"
                );
            }

            Transfer transfer = transferList.get(0);
            transfer.setBook(book);
            transfer.setSent(Instant.now());
            transfer.setStatus(Transfer.Status.PROCESSING);
            transfer.setMessage(request.getMessage());
            transfer.setTrackNumber(request.getTrackNumber());
            transferDao.save(transfer);

            getTransaction().commit();
            reply(new StatusResponse(true, "book sent successfully", request));
        } catch (RequestExecutionException e) {
            getTransaction().rollback();
            log.error(e.getWrapper().getLog());
            reject(e.getWrapper().getReply());
        } catch (Exception e) {
            log.error("Send book error on " + request.getClass().getSimpleName(), e);
            getTransaction().rollback();
            reject("internal server error");
        }
    }
}
