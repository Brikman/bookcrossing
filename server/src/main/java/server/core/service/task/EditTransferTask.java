package server.core.service.task;

import lombok.extern.log4j.Log4j2;
import server.cache.UserCache;
import server.core.service.ex.RequestExecutionException;
import server.dao.Dao;
import server.dao.TransferDao;
import server.model.entity.Transfer;
import server.net.dto.TransferDto;
import server.net.packet.request.EditTransferRequest;
import server.net.packet.response.StatusResponse;

@Log4j2
public class EditTransferTask extends ServerTask<EditTransferRequest> {

    @Dao
    private TransferDao transferDao;

    @Override
    public void process() {
        UserCache cache = UserCache.fromContext(context);
        TransferDto dto = request.getTransfer();

        try {
            getTransaction().begin();

            Transfer transfer = transferDao.find(dto.getId());
            if (transfer.getSender().getId() != cache.getId() && transfer.getStatus() != Transfer.Status.PROCESSING) {
                throw new RequestExecutionException("Not allowed to edit this transfer");
            }
            if (transfer != null) {
                String tracknumber = dto.getTrackNumber();
                String message = dto.getMessage();
                if (tracknumber != null && !tracknumber.trim().isEmpty()) {
                    transfer.setTrackNumber(tracknumber.trim());
                } else {
                    transfer.setTrackNumber(null);
                }
                if (message != null && !message.trim().isEmpty()) {
                    transfer.setMessage(message.trim());
                } else {
                    transfer.setMessage(null);
                }
                transferDao.save(transfer);
            } else {
                throw new RequestExecutionException("Wrong transfer id:" + dto.getId());
            }
            reply(new StatusResponse(true, "Edit transfer success", request));

            getTransaction().commit();
        } catch (RequestExecutionException e) {
            getTransaction().rollback();
            log.warn(e.getWrapper().getLog());
            reject(e.getWrapper().getReply());
        } catch (Exception e) {
            getTransaction().rollback();
            log.error(e);
            reject("Internal server error");
        }
    }
}
