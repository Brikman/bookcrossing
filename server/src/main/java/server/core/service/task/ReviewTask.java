package server.core.service.task;

import lombok.extern.log4j.Log4j2;
import server.cache.UserCache;
import server.core.service.ex.RequestExecutionException;
import server.dao.BookDao;
import server.dao.Dao;
import server.dao.ReviewDao;
import server.dao.UserDao;
import server.model.entity.Book;
import server.model.entity.Review;
import server.model.entity.User;
import server.net.packet.request.LeaveReviewRequest;
import server.net.packet.response.StatusResponse;

import java.util.List;
import java.util.Optional;


@Log4j2
public class ReviewTask extends ServerTask<LeaveReviewRequest> {

    @Dao
    private ReviewDao reviewDao;
    @Dao
    private BookDao bookDao;
    @Dao
    private UserDao userDao;

    @Override
    public void process() {
        UserCache cache = UserCache.fromContext(context);

        long bookId = request.getBookId();

        try {
            getTransaction().begin();

            Review review = reviewDao.findByUserAndBook(cache.getId(), bookId);
            if (review != null) {
                throw new RequestExecutionException("Review already exists");
            }
            List<Book> books = bookDao.findHistoryByUserId(cache.getId());
            Optional<Book> opt = books.stream().filter(book -> book.getId() == bookId).findFirst();
            if (!opt.isPresent()) {
                throw new RequestExecutionException("Review is not allowed for this book");
            }

            User user = userDao.find(cache.getId());
            Book book = opt.get();

            review = new Review();
            review.setUser(user);
            review.setBook(book);
            review.setComment(request.getComment());
            review.setRating(request.getRating() > 0 ? request.getRating() % 11 : 0);

            reviewDao.save(review);

            reply(StatusResponse.success("OK", request));
            getTransaction().commit();
        } catch (RequestExecutionException e) {
            getTransaction().rollback();
            log.warn(e.getWrapper().getLog());
            reject(e.getWrapper().getReply());
        } catch (Exception e) {
            getTransaction().rollback();
            log.error(e);
            reject("Internal server error");
        }
    }
}
