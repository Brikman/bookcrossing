package server.net.packet.request;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class ConfirmReceptionRequest extends Request {

    private long transferId;
    private String message;

    public ConfirmReceptionRequest(@JsonProperty("transferId") long transferId,
                                   @JsonProperty("message") String message,
                                   @JsonProperty("id") long id,
                                   @JsonProperty("code") byte code,
                                   @JsonProperty("version") byte version) {
        super(id, code, version);
        this.transferId = transferId;
        this.message = message;
    }
}
