package server.net.packet.request;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class RegistrationRequest extends Request {

    private String name;
    private String email;
    private String password;

    @JsonCreator
    public RegistrationRequest(@JsonProperty("name") String name,
                               @JsonProperty("email") String email,
                               @JsonProperty("password") String password,
                               @JsonProperty("id") long id,
                               @JsonProperty("code") byte code,
                               @JsonProperty("version") byte version) {
        super(id, code, version);
        this.name = name;
        this.email = email;
        this.password = password;
    }
}
