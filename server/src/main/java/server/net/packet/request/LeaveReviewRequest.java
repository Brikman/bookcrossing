package server.net.packet.request;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class LeaveReviewRequest extends Request {

    private long bookId;
    private String comment;
    private int rating;

    @JsonCreator
    protected LeaveReviewRequest(@JsonProperty("bookId") long bookId,
                                 @JsonProperty("comment") String comment,
                                 @JsonProperty("rating") int rating,
                                 @JsonProperty("id") long id,
                                 @JsonProperty("code") byte code,
                                 @JsonProperty("version") byte version) {
        super(id, code, version);
        this.bookId = bookId;
        this.comment = comment;
        this.rating = rating;
    }
}
