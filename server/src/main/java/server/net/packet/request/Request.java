package server.net.packet.request;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import server.net.packet.Packet;

@JsonIgnoreProperties(ignoreUnknown = true)
public abstract class Request extends Packet {

    protected Request(long id, byte code, byte version) {
        super(id, code, version);
    }
}
