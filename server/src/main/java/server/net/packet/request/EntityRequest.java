package server.net.packet.request;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class EntityRequest extends Request {

    private long entityId;
    private String entityName;

    @JsonCreator
    public EntityRequest(@JsonProperty("entityId") long entityId,
                         @JsonProperty("entityName") String entityName,
                         @JsonProperty("id") long id,
                         @JsonProperty("code") byte code,
                         @JsonProperty("version") byte version) {
        super(id, code, version);
        this.entityId = entityId;
        this.entityName = entityName;
    }
}
