package server.net.packet.request;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import server.net.dto.BookDto;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class SendBookRequest extends Request {

    private long recipientId;
    private BookDto book;
    private SendOption sendOption;
    private String message;
    private String trackNumber;

    @JsonCreator
    public SendBookRequest(@JsonProperty("book") BookDto book,
                           @JsonProperty("recipientId") long recipientId,
                           @JsonProperty("sendOption") SendOption option,
                           @JsonProperty("message") String message,
                           @JsonProperty("trackNumber") String trackNumber,
                           @JsonProperty("id") long id,
                           @JsonProperty("code") byte code,
                           @JsonProperty("version") byte version) {
        super(id, code, version);
        this.recipientId = recipientId;
        this.book = book;
        this.message = message;
        this.trackNumber = trackNumber;
        this.sendOption = option;
    }

    public enum SendOption {
        SEND_NEW, FORWARD
    }
}
