package server.net.packet.request;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import server.net.dto.TransferDto;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class EditTransferRequest extends Request {

    private TransferDto transfer;

    @JsonCreator
    public EditTransferRequest(@JsonProperty("transfer") TransferDto transfer,
                               @JsonProperty("id") long id,
                               @JsonProperty("code") byte code,
                               @JsonProperty("version") byte version) {
        super(id, code, version);
        this.transfer = transfer;
    }
}
