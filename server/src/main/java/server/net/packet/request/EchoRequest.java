package server.net.packet.request;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class EchoRequest extends Request {

    private String message;

    @JsonCreator
    protected EchoRequest(@JsonProperty("id") long id,
                          @JsonProperty("code") byte code,
                          @JsonProperty("version") byte version,
                          @JsonProperty("message") String message) {
        super(id, code, version);
        this.message = message;
    }
}
