package server.net.packet.request;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import server.net.packet.PacketRegistry;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class AuthRequest extends Request {

    private String email;
    private String password;

    @JsonCreator
    public AuthRequest(@JsonProperty("email") String email,
                       @JsonProperty("password") String password,
                       @JsonProperty("id") long id,
                       @JsonProperty("code") byte code,
                       @JsonProperty("version") byte version) {
        super(id, code, version);
        this.email = email;
        this.password = password;
    }
}
