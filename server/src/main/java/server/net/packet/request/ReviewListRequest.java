package server.net.packet.request;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class ReviewListRequest extends Request {

    private long bookId;

    @JsonCreator
    protected ReviewListRequest(@JsonProperty("bookId") long bookId,
                                @JsonProperty("id") long id,
                                @JsonProperty("code") byte code,
                                @JsonProperty("version") byte version) {
        super(id, code, version);
        this.bookId = bookId;
    }
}
