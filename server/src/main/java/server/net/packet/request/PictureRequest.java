package server.net.packet.request;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class PictureRequest extends Request {

    private String entityName;
    private long entityId;

    @JsonCreator
    protected PictureRequest(@JsonProperty("entityName") String entityName,
                             @JsonProperty("entityId") long entityId,
                             @JsonProperty("id") long id,
                             @JsonProperty("code") byte code,
                             @JsonProperty("version") byte version) {
        super(id, code, version);
        this.entityName = entityName;
        this.entityId = entityId;
    }
}
