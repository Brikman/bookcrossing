package server.net.packet.request;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class TransferListRequest extends Request {

    private TransferListOption option;

    @JsonCreator
    public TransferListRequest(@JsonProperty("option") TransferListOption option,
                               @JsonProperty("id") long id,
                               @JsonProperty("code") byte code,
                               @JsonProperty("version") byte version) {
        super(id, code, version);
        this.option = option;
    }

    public enum TransferListOption {
        OUTBOUND, INCOMING
    }
}
