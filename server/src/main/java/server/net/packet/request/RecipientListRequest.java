package server.net.packet.request;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class RecipientListRequest extends Request {

    public static final int LIST_NEW = 0;
    public static final int LIST_PENDING = 1;

    private int option;

    @JsonCreator
    public RecipientListRequest(@JsonProperty("option") int option,
                                @JsonProperty("id") long id,
                                @JsonProperty("code") byte code,
                                @JsonProperty("version") byte version) {
        super(id, code, version);
        this.option = option;
    }

    public int getOption() {
        return option;
    }
}
