package server.net.packet.request;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import lombok.Data;
import server.codec.decoder.EditAccountRequestDeserializer;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Data
@JsonDeserialize(using = EditAccountRequestDeserializer.class)
public class EditAccountRequest extends Request {

    private Map<String, Object> fields;

    public EditAccountRequest(Map<String, Object> fields, long id, byte code, byte version) {
        super(id, code, version);
        this.fields = fields;
    }

    public boolean hasChanged(Field field) {
        return fields.containsKey(field.getName());
    }

    public Object getField(Field field) {
        return fields.get(field.getName());
    }

    public enum Field {

        NAME("name"),
        EMAIL("email"),
        PHONE("phone"),
        ADDRESS("address"),
        ABOUT("about"),
        PICTURE("picture");

        String name;

        Field(String name) {
            this.name = name;
        }

        public String getName() {
            return name;
        }

        public static List<String> listNames() {
            return Arrays.stream(values()).map(Field::getName).collect(Collectors.toList());
        }
    }
}
