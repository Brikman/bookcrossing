package server.net.packet.request;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import server.net.dto.BookDto;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class EditBookRequest extends Request {

    private BookDto book;

    @JsonCreator
    protected EditBookRequest(@JsonProperty("book") BookDto book,
                              @JsonProperty("id") long id,
                              @JsonProperty("code") byte code,
                              @JsonProperty("version") byte version) {
        super(id, code, version);
        this.book = book;
    }
}
