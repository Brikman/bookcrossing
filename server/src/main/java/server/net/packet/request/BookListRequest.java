package server.net.packet.request;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class BookListRequest extends Request {

    private String condition;
    private String value;

    @JsonCreator
    public BookListRequest(@JsonProperty("condition") String condition,
                           @JsonProperty("value") String value,
                           @JsonProperty("id") long id,
                           @JsonProperty("code") byte code,
                           @JsonProperty("version") byte version) {
        super(id, code, version);
        this.condition = condition;
        this.value = value;
    }
}
