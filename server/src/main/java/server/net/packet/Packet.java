package server.net.packet;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import server.net.packet.request.Request;
import server.net.packet.response.Response;

import java.io.Serializable;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public abstract class Packet implements Serializable {

    private long id;
    private byte code;
    private byte version;

    protected Packet(long id, byte code, byte version) {
        this.id = id;
        this.version = version;
        this.code = code;
    }

    protected Packet(long id, byte version) {
        this.id = id;
        this.version = version;
        this.code = PacketRegistry.getPacketCode(this);
    }

    @JsonIgnore
    public byte getType() {
        if (this instanceof Request)
            return 1;
        if (this instanceof Response)
            return 2;
        return -1;
    }
}
