package server.net.packet.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import server.net.packet.request.Request;

public class StatusResponse extends Response {

    private boolean status;
    private String message;

    public static StatusResponse success(String message, Request request) {
        return new StatusResponse(true, message, request);
    }

    public static StatusResponse fail(String message, Request request) {
        return new StatusResponse(false, message, request);
    }

    public StatusResponse(boolean status, String message, Request request) {
        super(request);
        this.status = status;
        this.message = message;
    }

    @JsonProperty("status")
    public boolean isSuccess() {
        return status;
    }
    public void setStatus(boolean status) {
        this.status = status;
    }
    public String getMessage() {
        return message;
    }
    public void setMessage(String message) {
        this.message = message;
    }
}