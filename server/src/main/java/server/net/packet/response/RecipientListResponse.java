package server.net.packet.response;

import lombok.Data;
import server.net.dto.UserDto;
import server.net.packet.request.Request;

import java.util.List;

@Data
public class RecipientListResponse extends Response {

    private List<UserDto> recipients;
    private int availableRecipients;

    public RecipientListResponse(List<UserDto> recipients, int availableRecipients, Request request) {
        super(request);
        this.recipients = recipients;
        this.availableRecipients = availableRecipients;
    }
}
