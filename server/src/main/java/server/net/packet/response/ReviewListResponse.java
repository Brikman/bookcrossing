package server.net.packet.response;

import lombok.Data;
import server.net.dto.BookDto;
import server.net.dto.ReviewDto;
import server.net.packet.request.Request;

import java.util.List;

@Data
public class ReviewListResponse extends Response {

    private final BookDto book;
    private final List<ReviewDto> reviews;

    public ReviewListResponse(BookDto book, List<ReviewDto> reviews, Request request) {
        super(request);
        this.book = book;
        this.reviews = reviews;
    }
}
