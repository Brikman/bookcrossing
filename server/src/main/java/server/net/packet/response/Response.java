package server.net.packet.response;

import server.net.packet.Packet;
import server.net.packet.PacketRegistry;
import server.net.packet.request.Request;

public abstract class Response extends Packet {

    protected Response(Request request) {
        this(request.getId(), request.getVersion());
    }

    protected Response(long id, byte version) {
        super(id, version);
    }
}
