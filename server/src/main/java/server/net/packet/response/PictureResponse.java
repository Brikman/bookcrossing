package server.net.packet.response;

import lombok.Data;
import server.model.entity.Picture;
import server.net.packet.request.Request;

@Data
public class PictureResponse extends Response {

    private Picture picture;

    public PictureResponse(Picture picture, Request request) {
        super(request);
        this.picture = picture;
    }
}
