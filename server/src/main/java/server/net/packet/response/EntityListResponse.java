package server.net.packet.response;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Data;
import server.codec.encoder.EntityListResponseSerializer;
import server.net.dto.EntityDto;
import server.net.packet.request.Request;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Data
@JsonSerialize(using = EntityListResponseSerializer.class)
public class EntityListResponse<T extends EntityDto> extends Response {

    private List<T> entities;

    @JsonCreator
    public EntityListResponse(Collection<T> entities, Request request) {
        super(request);
        this.entities = new ArrayList<>(entities);
    }
}
