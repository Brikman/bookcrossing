package server.net.packet.response;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Data;
import server.codec.encoder.EntityResponseSerializer;
import server.net.dto.EntityDto;
import server.net.packet.request.Request;

@Data
@JsonSerialize(using = EntityResponseSerializer.class)
public class EntityResponse<T extends EntityDto> extends Response {

    private final T entity;

    public EntityResponse(T entity, Request request) {
        super(request);
        this.entity = entity;
    }
}
