package server.net.packet.response;

import lombok.Data;
import server.net.packet.request.Request;

@Data
public class RejectionPacket extends Response {

    private String message;

    public RejectionPacket(Request request) {
        this(null, request);
    }

    public RejectionPacket(long id, byte version, String message) {
        super(id, version);
        this.message = message;
    }

    public RejectionPacket(String message, Request request) {
        super(request);
        this.message = message;
    }
}
