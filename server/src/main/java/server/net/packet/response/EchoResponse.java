package server.net.packet.response;

import lombok.Data;
import server.net.packet.request.EchoRequest;

@Data
public class EchoResponse extends Response {

    private String message;

    public EchoResponse(EchoRequest request) {
        super(request);
        this.message = request.getMessage();
    }
}
