package server.net.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class ReviewDto implements EntityDto {

    private long id;
    private UserDto user;
    private BookDto book;
    private String comment;
    private int rating;
}
