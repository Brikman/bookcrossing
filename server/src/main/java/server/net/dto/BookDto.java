package server.net.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import server.model.entity.Picture;

import java.util.Collections;
import java.util.List;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class BookDto implements EntityDto {

    private long id;
    private String title;
    private String description;
    private String author;
    private Picture picture;
    private float rating;
}
