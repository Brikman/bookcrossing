package server.net.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import java.time.Instant;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class TransferDto implements EntityDto {

    private long id;
    private UserDto sender;
    private UserDto recipient;
    private BookDto book;
    private Instant sent;
    private String message;
    private String trackNumber;
}
