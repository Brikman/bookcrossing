package server.net.dto;

import lombok.extern.log4j.Log4j2;
import server.model.entity.*;

import java.util.*;
import java.util.stream.Collectors;

@Log4j2
public class DtoBuilder {

    public static UserDto of(User user) {
        if (user == null)
            return null;
        UserDto dto = new UserDto();
        dto.setId(user.getId());
        dto.setName(user.getName());
        dto.setEmail(user.getEmail());
        dto.setPhone(user.getPhone());
        dto.setAbout(user.getAbout());
        dto.setAddress(of(user.getAddress()));
        return dto;
    }

    public static BookDto of(Book book) {
        if (book == null)
            return null;
        BookDto dto = new BookDto();
        dto.setId(book.getId());
        dto.setTitle(book.getTitle());
        dto.setDescription(book.getDescription());
        dto.setAuthor(book.getAuthor());
        dto.setRating(book.getRating());
        return dto;
    }

    public static AddressDto of(Address address) {
        if (address == null)
            return null;
        AddressDto dto = new AddressDto();
        dto.setCountryCode(address.getCountryCode());
        dto.setCountryName(address.getCountryName());
        dto.setCityCode(address.getCityCode());
        dto.setCityName(address.getCityName());
        dto.setStreet(address.getStreet());
        dto.setHouse(address.getHouse());
        dto.setApartment(address.getApartment());
        dto.setPostalCode(address.getPostalCode());
        return dto;
    }

    public static TransferDto of(Transfer transfer) {
        if (transfer == null)
            return null;
        TransferDto dto = new TransferDto();
        dto.setId(transfer.getId());
        dto.setSender(of(transfer.getSender()));
        dto.setRecipient(of(transfer.getRecipient()));
        dto.setBook(of(transfer.getBook()));
        dto.setSent(transfer.getSent());
        dto.setMessage(transfer.getMessage());
        dto.setTrackNumber(transfer.getTrackNumber());
        return dto;
    }

    public static ReviewDto of(Review review) {
        if (review == null)
            return null;
        ReviewDto dto = new ReviewDto();
        dto.setId(review.getId());
        dto.setUser(of(review.getUser()));
        dto.setBook(of(review.getBook()));
        dto.setRating(review.getRating());
        dto.setComment(review.getComment());
        return dto;
    }

    public static List<EntityDto> listOf(Collection<? extends ModelEntity> entities) {
        if (entities == null) {
            return Collections.emptyList();
        }
        return entities.stream().map(DtoBuilder::of).collect(Collectors.toList());
    }

    public static EntityDto of(ModelEntity entity) {
        if (entity instanceof User) {
            return of((User) entity);
        } else if (entity instanceof Book) {
            return of((Book) entity);
        } else if (entity instanceof Address) {
            return of((Address) entity);
        } else if (entity instanceof Transfer) {
            return of((Transfer) entity);
        }
        throw new IllegalArgumentException("No corresponding dto class for given entity: "
                + entity.getClass().getSimpleName());
    }
}
