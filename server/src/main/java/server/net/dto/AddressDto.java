package server.net.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import lombok.extern.log4j.Log4j2;

@Data
@Log4j2
@JsonIgnoreProperties(ignoreUnknown = true)
public class AddressDto implements EntityDto {

    private String countryCode;
    private String countryName;
    private String cityCode;
    private String cityName;
    private String street;
    private String house;
    private String apartment;
    private String postalCode;
}
