package server.net.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import server.model.entity.Picture;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class UserDto implements EntityDto {

    private long id;
    private String name;
    private String email;
    private String phone;
    private AddressDto address;
    private String about;
    private Picture picture;
}
