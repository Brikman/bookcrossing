package server.cache;

import io.netty.channel.ChannelHandlerContext;

public class ChannelCache {

    private UserCache user = new UserCache();

    public ChannelCache(ChannelHandlerContext context) {
        context.channel().attr(AttributeKeys.CHANNEL_CACHE).set(this);
    }

    public UserCache getUser() {
        return user;
    }

    public void setUser(UserCache user) {
        this.user = user;
    }
}
