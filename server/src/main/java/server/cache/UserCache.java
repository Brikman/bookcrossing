package server.cache;

import io.netty.channel.ChannelHandlerContext;
import lombok.Data;
import server.model.entity.User;

@Data
public class UserCache {

    private long id;
    private String email;
    private String name;

    public void update(User user) {
        id = user.getId();
        email = user.getEmail();
        name = user.getName();
    }

    public static UserCache fromContext(ChannelHandlerContext context) {
        return context.channel().attr(AttributeKeys.CHANNEL_CACHE).get().getUser();
    }
}
