package server.util;

public class ErrorWrapper {
    private String log = "";
    private String reply = "";
    private Exception cause;

    public ErrorWrapper(String log) {
        this.log = log;
        this.reply = log;
    }

    public ErrorWrapper(String log, String reply) {
        this.log = log;
        this.reply = reply;
    }

    public ErrorWrapper(String log, String reply, Exception cause) {
        this.log = log;
        this.reply = reply;
        this.cause = cause;
    }

    public String getLog() {
        return log;
    }
    public String getReply() {
        return reply;
    }
    public Exception getCause() {
        return cause;
    }
}
