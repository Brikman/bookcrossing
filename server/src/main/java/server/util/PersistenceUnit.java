package server.util;

public class PersistenceUnit {

    public static final String LOCAL = "bookcrossing-local";
    public static final String REMOTE = "bookcrossing-remote";

    public static final String CURRENT = LOCAL;
}
