package server;

import server.core.Server;

import java.net.InetSocketAddress;

public class ServerMain {

    private static final int port = 9000;
    private static Server server;

    public static void main(String[] args) {
        InetSocketAddress address = new InetSocketAddress(port);
        server = new Server(address);
        server.start();
    }
}
