import client.core.Client;
import client.future.RejectionException;
import client.future.ResponseFuture;
import client.net.dto.*;
import client.net.packet.request.EditAccountRequest;
import client.net.packet.response.*;
import lombok.extern.log4j.Log4j2;

import java.net.ConnectException;
import java.util.List;
import java.util.concurrent.TimeoutException;

@Log4j2
public class ClientMain {

    private static Client client = new Client();

    public static void main(String[] args) {
        try {
            String s = "admin";

            User user = doLogin(s + "@gmail.com", s);

//            doGetBookList(user.getId());
//            System.out.println("=========");
//            doGetBookHistory();
//            doLeaveReview(1, "Bullshit", 3);

            doGetBookList(1);

//            doGetBook(1);

//            doGetReviewList(1);

            //doGetBookOwners(1);

        } catch (RejectionException e) {
            log.error("Request rejected: " + e.getPacket().getMessage());
        } catch (TimeoutException e) {
            log.error("Request timeout exceeded");
        } catch (RuntimeException e) {
            if (e.getCause() instanceof ConnectException) {
                log.error("Connection failed: cannot discover server", e);
            } else {
                log.error("Runtime exception", e);
            }
        } catch (Exception e) {
            log.error("Exception caught", e);
        } finally {
            try {
                doExit();
            } catch (Exception e) {
                log.error(e);
            }
        }
    }

    private static void doLogout() {
        client.logout();
    }

    public static User doLogin(String login, String password)
            throws RejectionException, TimeoutException, InterruptedException
    {
        ResponseFuture<EntityResponse<User>> future = client.login(login, password);
        EntityResponse<User> response = future.get();
        log.info("Login SUCCESS: server.client id:" + response.getEntity().getId());
        return response.getEntity();
    }

    public static User doRegistration(String name, String login, String password)
            throws RejectionException, TimeoutException, InterruptedException
    {
        ResponseFuture<EntityResponse<User>> future = client.register(login, password, name);
        EntityResponse<User> response = future.get();
        log.info("Registration SUCCESS: server.client id=" + response.getEntity().getId());
        return response.getEntity();
    }

    public static void doEditAccount(EditAccountRequest request)
            throws InterruptedException, RejectionException, TimeoutException {
        ResponseFuture<StatusResponse> future = client.editAccount(request);
        StatusResponse response = future.get();
        System.out.println(response);
    }

    public static void doGetUser(long userId)
            throws RejectionException, TimeoutException, InterruptedException
    {
        ResponseFuture<EntityResponse<User>> future = client.getUser(userId);
        EntityResponse<User> response = future.get();
        User user = response.getEntity();
        System.out.println(user);
    }

    public static void doGetBook(long bookId)
            throws RejectionException, TimeoutException, InterruptedException
    {
        ResponseFuture<EntityResponse<Book>> future = client.getBook(bookId);
        EntityResponse<Book> response = future.get();
        Book book = response.getEntity();
        System.out.println(book);
    }

    public static void doGetBookList(long userId)
            throws RejectionException, TimeoutException, InterruptedException
    {
        ResponseFuture<EntityListResponse<Book>> future = client.getBookListByUser(userId);
        EntityListResponse<Book> response = future.get();
        List<Book> books = response.getEntities();
        System.out.println("books: " + books.size());
        books.forEach(book -> {
            System.out.println(book.getId() + " (" + book.getRating() + ") : " + book.getTitle());
        });
    }

    private static Picture doGetPicture(Entity entity)
            throws InterruptedException, RejectionException, TimeoutException
    {
        ResponseFuture<PictureResponse> future = client.getPicture(entity);
        PictureResponse response = future.get();
        return response.getPicture();
    }

    public static void doSendBook(long userId, String bookName, String authorName, String message, String trackNumber)
            throws InterruptedException, RejectionException, TimeoutException
    {
        Book book = new Book();
        book.setTitle(bookName);
        book.setAuthor(authorName);
        ResponseFuture<StatusResponse> future = client.sendBook(book, userId, message, trackNumber);
        StatusResponse response = future.get();
        log.info(response);
    }

    public static void doSendBook(long userId, long bookId, String message, String trackNumber)
            throws InterruptedException, RejectionException, TimeoutException
    {
        Book book = new Book();
        book.setId(bookId);
        ResponseFuture<StatusResponse> future = client.sendBook(book, userId, message, null);
        StatusResponse response = future.get();
        log.info(response);
    }

    public static void doRequireRecipients()
            throws InterruptedException, RejectionException, TimeoutException {
        ResponseFuture<RecipientListResponse> future = client.requireRecipients();
        RecipientListResponse response = future.get();
        response.getRecipients().forEach(e -> System.out.println(e.getId() + " : " + e.getName()));
    }

    public static void doGetRecipientList()
            throws InterruptedException, RejectionException, TimeoutException {
        ResponseFuture<RecipientListResponse> future = client.getRecipientsList();
        RecipientListResponse response = future.get();
        response.getRecipients().forEach(e -> System.out.println(e.getId() + " : " + e.getName()));
    }

    public static void doGetIncomingTransferList()
            throws InterruptedException, RejectionException, TimeoutException {
        ResponseFuture<EntityListResponse<Transfer>> future = client.getIncomingTransferList();
        EntityListResponse<Transfer> transfers = future.get();
        transfers.getEntities().forEach(System.out::println);
    }

    public static void doGetOutboundTransferList()
            throws InterruptedException, RejectionException, TimeoutException {
        ResponseFuture<EntityListResponse<Transfer>> future = client.getOutboundTransferList();
        EntityListResponse<Transfer> transfers = future.get();
        transfers.getEntities().forEach(System.out::println);
    }

    public static void doConfirmTransfer(long transferId, String message)
            throws InterruptedException, RejectionException, TimeoutException {
        ResponseFuture<StatusResponse> future = client.confirmTransfer(transferId, message);
        StatusResponse response = future.get();
        System.out.println(response);
    }

    public static void doGetBookOwners(long bookId)
            throws InterruptedException, RejectionException, TimeoutException {
        ResponseFuture<EntityListResponse<User>> future = client.getBookOwners(bookId);
        EntityListResponse<User> response = future.get();
        for (User user : response.getEntities()) {
            System.out.println(user.getId() + " : " + user.getName());
        }
    }

    public static void doGetBookHistory()
            throws InterruptedException, RejectionException, TimeoutException {
        ResponseFuture<EntityListResponse<Book>> future = client.getBookHistoryList();
        EntityListResponse<Book> response = future.get();
        List<Book> books = response.getEntities();
        System.out.println("books: " + books.size());
        books.forEach(System.out::println);
    }

    public static void doLeaveReview(long bookId, String comment, int rating)
            throws InterruptedException, RejectionException, TimeoutException {
        ResponseFuture<StatusResponse> future = client.leaveReview(bookId, comment, rating);
        System.out.println(future.get());
    }

    public static void doGetReviewList(long bookId)
            throws InterruptedException, RejectionException, TimeoutException {
        ResponseFuture<ReviewListResponse> future = client.getReviewList(bookId);

        for (Review review : future.get().getReviews()) {
            System.out.println(
                    ">> " + review.getUser().getName()
                    + " (" + review.getRating() + ") : "
                    + review.getComment()
            );
        }
    }

    public static void doExit() {
        log.info("Closing client...");
        client.logout();
    }
}
