package client.future;

import client.net.packet.response.Response;

public interface ResponseFutureListener<T extends Response> {

    void accepted(ResponseFuture<T> future);
}
