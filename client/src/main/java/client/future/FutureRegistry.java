package client.future;

import client.core.Connection;
import client.net.packet.request.Request;
import client.net.packet.response.Response;

import java.util.Iterator;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class FutureRegistry {

    private volatile Map<Request, ResponseFuture> futures = new ConcurrentHashMap<>();

    public synchronized ResponseFuture futureFor(Request request, Connection connection) {
        if (futures.containsKey(request))
            return futures.get(request);
        ResponseFuture future = new ResponseFuture(this, connection);
        futures.put(request, future);
        return future;
    }

    public synchronized void release(ResponseFuture future) {
        Iterator<Map.Entry<Request, ResponseFuture>> iterator = futures.entrySet().iterator();
        while (iterator.hasNext()) {
            Map.Entry<Request, ResponseFuture> entry = iterator.next();
            if (entry.getValue().equals(future)) {
                iterator.remove();
                return;
            }
        }
    }

    public synchronized boolean notifyFuture(Response response) {
        Iterator<Map.Entry<Request, ResponseFuture>> iterator = futures.entrySet().iterator();
        while (iterator.hasNext()) {
            Map.Entry<Request, ResponseFuture> entry = iterator.next();
            Request request = entry.getKey();
            ResponseFuture future = entry.getValue();
            if (request.getId() == response.getId()) {
                iterator.remove();
                future.put(response);
                return true;
            }
        }
        return false;
    }

    public synchronized Request getRequest(ResponseFuture future) {
        for (Map.Entry<Request, ResponseFuture> entry : futures.entrySet()) {
            if (entry.getValue().equals(future)) {
                return entry.getKey();
            }
        }
        return null;
    }

    public synchronized void cancelFuture(Request request) {
        futures.get(request).cancel();
    }
}
