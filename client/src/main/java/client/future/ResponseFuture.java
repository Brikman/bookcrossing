package client.future;

import client.core.Connection;
import client.net.packet.response.RejectionPacket;
import client.net.packet.response.Response;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;


public class ResponseFuture<T extends Response> {

    private static final long DEFAULT_TIMEOUT_MS = 10_000;

    private final FutureRegistry registry;
    private final Connection connection;
    private final CountDownLatch latch = new CountDownLatch(1);
    private volatile T value;

    private boolean isCancelled = false;
    private boolean isDone      = false;

    private ResponseFutureListener<T> listener;

    ResponseFuture(FutureRegistry registry, Connection connection) {
        this.registry = registry;
        this.connection = connection;
    }

    public boolean cancel() {
        if (!isDone) {
            isCancelled = true;
            registry.release(this);
            latch.countDown();
            return true;
        }
        return false;
    }

    public boolean isCancelled() {
        return isCancelled;
    }

    public boolean isDone() {
        return latch.getCount() == 0 && !isCancelled;
    }

    public T get() throws InterruptedException, RejectionException, TimeoutException {
        connection.force(this);
        if (latch.await(DEFAULT_TIMEOUT_MS, TimeUnit.MILLISECONDS)) {
            if (value instanceof RejectionPacket)
                throw new RejectionException((RejectionPacket) value);
            return value;
        }
        throw new TimeoutException();
    }

    public T get(long timeout, TimeUnit unit) throws TimeoutException, InterruptedException, RejectionException {
        if (latch.await(timeout, unit)) {
            return get();
        }
        throw new TimeoutException();
    }

    public ResponseFuture<T> sync() throws InterruptedException {
        latch.await();
        return this;
    }

    public void addListener(ResponseFutureListener<T> listener) {
        this.listener = listener;
    }

    void put(T value) {
        this.value = value;
        latch.countDown();
        isDone = true;
        if (listener != null) {
            listener.accepted(this);
        }
    }
}
