package client.core;

import client.future.FutureRegistry;
import client.net.packet.response.Response;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import lombok.extern.log4j.Log4j2;

import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Log4j2
class InboundHandler extends ChannelInboundHandlerAdapter {

    private ExecutorService executor = Executors.newFixedThreadPool(5);
    private FutureRegistry futures;

    public InboundHandler(FutureRegistry futures) {
        this.futures = futures;
    }

    @Override
    public void channelRead(ChannelHandlerContext context, Object msg) {
        log.info("Message received: " + msg.getClass().getSimpleName());
        if (msg instanceof Response) {
            executor.execute(() -> futures.notifyFuture((Response) msg));
        }
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext context, Throwable cause) {
        log.error("Exception caught", cause);
        context.close();
    }

    @Override
    public void channelUnregistered(ChannelHandlerContext context) throws Exception {
        List<Runnable> unexecuted = executor.shutdownNow();
        log.info("Channel unregistered" + (!unexecuted.isEmpty() ? ": " + unexecuted.size() + " unexecuted" : ""));
        super.channelUnregistered(context);
    }
}
