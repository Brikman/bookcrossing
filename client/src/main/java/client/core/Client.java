package client.core;

import client.future.ResponseFuture;
import client.future.ResponseFutureListener;
import client.net.dto.*;
import client.net.packet.request.*;
import client.net.packet.response.*;
import lombok.extern.log4j.Log4j2;

import java.net.InetSocketAddress;

@Log4j2
public class Client {

    private Connection connection;
    private long clientId = -1;

    private InetSocketAddress serverAddress = new InetSocketAddress("localhost", 9000);

    private boolean autoflush = true;

    public Client() {
    }

    public void connect() {
        if (!isConnected()) {
            connection = new Connection(serverAddress);
            connection.setAutoflush(autoflush);
        }
    }

    public ResponseFuture<EntityResponse<User>> login(String login, String password) {
        AuthRequest request = new AuthRequest(login, password);
        connect();
        ResponseFuture<EntityResponse<User>> future = connection.send(request);
        future.addListener(new ResponseFutureListener<EntityResponse<User>>() {
            @Override
            public void accepted(ResponseFuture<EntityResponse<User>> future) {
                try {
                    clientId = future.get().getEntity().getId();
                } catch (Exception ignored) {
                }
            }
        });
        return future;
    }

    public ResponseFuture<EntityResponse<User>> register(String login, String password, String name) {
        connect();
        RegistrationRequest request = new RegistrationRequest(name, login, password);
        ResponseFuture<EntityResponse<User>> future = connection.send(request);
        future.addListener(new ResponseFutureListener<EntityResponse<User>>() {
            @Override
            public void accepted(ResponseFuture<EntityResponse<User>> future) {
                try {
                    clientId = future.get().getEntity().getId();
                } catch (Exception ignored) {
                }
            }
        });
        return future;
    }

    public ResponseFuture<StatusResponse> editAccount(EditAccountRequest request) {
        return connection.send(request);
    }

    public ResponseFuture<EntityResponse<User>> getUser() {
        return getUser(clientId);
    }

    public ResponseFuture<EntityResponse<User>> getUser(long userId) {
        EntityRequest request = new EntityRequest(userId, EntityRequest.USER);
        return connection.send(request);
    }

    public ResponseFuture<EntityResponse<Book>> getBook(long bookId) {
        EntityRequest request = new EntityRequest(bookId, EntityRequest.BOOK);
        return connection.send(request);
    }

    public ResponseFuture<EntityListResponse<Book>> getBookHistoryList() {
        BookListRequest request = BookListRequest.history();
        return connection.send(request);
    }

    public ResponseFuture<EntityListResponse<Book>> getBookListByUser() {
        return getBookListByUser(clientId);
    }

    public ResponseFuture<EntityListResponse<Book>> getBookListByUser(long userId) {
        BookListRequest request = BookListRequest.byUser(userId);
        return connection.send(request);
    }

    public ResponseFuture<EntityListResponse<Book>> getBookListByTitle(String title) {
        BookListRequest request = BookListRequest.byTitle(title);
        return connection.send(request);
    }

    public ResponseFuture<PictureResponse> getPicture(Entity entity) {
        PictureRequest request = new PictureRequest(entity);
        return connection.send(request);
    }

    public ResponseFuture<RecipientListResponse> requireRecipients() {
        RecipientListRequest request = RecipientListRequest.forNewList();
        return connection.send(request);
    }

    public ResponseFuture<RecipientListResponse> getRecipientsList() {
        RecipientListRequest request = RecipientListRequest.forPendingList();
        return connection.send(request);
    }

    public ResponseFuture<StatusResponse> sendBook(Book book, long recipientId, String message, String trackNumber) {
        SendBookRequest request = new SendBookRequest(book, recipientId, SendOption.SEND_NEW, message, trackNumber);
        return connection.send(request);
    }

    public ResponseFuture<StatusResponse> sendBook(long bookId, long recipientId, String message, String trackNumber) {
        Book book = new Book();
        book.setId(bookId);
        SendBookRequest request = new SendBookRequest(book, recipientId, SendOption.FORWARD, message, trackNumber);
        return connection.send(request);
    }

    public ResponseFuture<EntityListResponse<Transfer>> getOutboundTransferList() {
        TransferListRequest request = new TransferListRequest(TransferListOption.OUTBOUND);
        return connection.send(request);
    }

    public ResponseFuture<EntityListResponse<Transfer>> getIncomingTransferList() {
        TransferListRequest request = new TransferListRequest(TransferListOption.INCOMING);
        return connection.send(request);
    }

    public ResponseFuture<StatusResponse> confirmTransfer(long transferId, String message) {
        ConfirmReceptionRequest request = new ConfirmReceptionRequest(transferId, message);
        return connection.send(request);
    }

    public ResponseFuture<EntityListResponse<User>> getBookOwners(long bookId) {
        BookOwnersRequest request = new BookOwnersRequest(bookId);
        return connection.send(request);
    }

    public ResponseFuture<StatusResponse> leaveReview(long bookId, String comment, int rating) {
        LeaveReviewRequest request = new LeaveReviewRequest(bookId, comment, rating);
        return connection.send(request);
    }

    public ResponseFuture<ReviewListResponse> getReviewList(long bookId) {
        
        ReviewListRequest request = new ReviewListRequest(bookId);
        return connection.send(request);
    }

    public ResponseFuture<EchoResponse> echo(String message) {
        return connection.send(new EchoRequest(message));
    }

    public void sendNoResponse(Request request) {
        connection.sendNoResponse(request);
    }

    public long getId() {
        return clientId;
    }

    public void flush() {
        connection.flush();
    }

    public void setAutoflush(boolean flag) {
        this.autoflush = flag;
        if (connection != null) {
            connection.setAutoflush(flag);
            connection.flush();
        }
    }

    public boolean isConnected() {
        return connection != null && connection.isOpen();
    }

    public void logout() {
        if (connection != null)
            connection.close();
    }
}
