package client.core;

import client.codec.Decoder;
import client.codec.Encoder;
import client.future.FutureRegistry;
import client.future.ResponseFuture;
import client.net.packet.request.Request;
import io.netty.bootstrap.Bootstrap;
import io.netty.channel.Channel;
import io.netty.channel.ChannelException;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioSocketChannel;
import lombok.extern.log4j.Log4j2;

import java.net.SocketAddress;
import java.util.LinkedList;
import java.util.Queue;

@Log4j2
public class Connection {

    private Channel channel;
    private Queue<Request> requestQueue = new LinkedList<>();
    private FutureRegistry futureRegistry;

    private boolean autoflush = true;

    private final byte version = 1;

    public Connection(SocketAddress serverAddress) {
        try {
            futureRegistry = new FutureRegistry();
            NioEventLoopGroup workerGroup = new NioEventLoopGroup();
            Bootstrap bootstrap = new Bootstrap()
                    .channel(NioSocketChannel.class)
                    .group(workerGroup)
                    .remoteAddress(serverAddress)
                    .handler(new ChannelInitializer<SocketChannel>() {
                        @Override
                        protected void initChannel(SocketChannel channel) throws Exception {
                            channel.pipeline()
                                    .addLast(new Encoder())
                                    .addLast(new Decoder())
                                    .addLast(new InboundHandler(futureRegistry));
                        }
                    });
            channel = bootstrap.connect().sync().channel();
        } catch (Exception e) {
            throw new ChannelException(e);
        }
    }

    private void connect(SocketAddress serverAddress) {
    }

    public ResponseFuture send(Request request) {
        if (channel == null)
            throw new ChannelException("Connection has not been established");
        if (!isOpen())
            throw new ChannelException("Connection closed");
        request.setVersion((byte) 1);
        if (autoflush) {
            channel.writeAndFlush(request);
        } else {
            requestQueue.offer(request);
        }
        return futureRegistry.futureFor(request, this);
    }

    public void sendNoResponse(Request request) {
        if (channel == null)
            throw new ChannelException("The connection has not been established");
        if (!isOpen())
            throw new ChannelException("Connection closed");
        request.setVersion(version);
        channel.writeAndFlush(request);
    }

    public void flush() {
        if (isOpen()) {
            while (!requestQueue.isEmpty()) {
                Request request = requestQueue.poll();
                channel.writeAndFlush(request);
            }
        }
    }

    public void force(ResponseFuture future) {
        Request request = futureRegistry.getRequest(future);
        if (request != null && requestQueue.remove(request)) {
            channel.writeAndFlush(request);
        }
    }

    public void close() {
        requestQueue.clear();
        if (channel != null) {
            channel.eventLoop().parent().shutdownGracefully();
            channel.close();
        }
    }

    public boolean isOpen() {
        return channel != null && channel.isOpen();
    }

    public void setAutoflush(boolean flag) {
        this.autoflush = flag;
    }
}
