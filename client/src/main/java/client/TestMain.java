package client;

import client.core.Client;
import client.future.RejectionException;
import client.future.ResponseFuture;
import client.net.dto.User;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

public class TestMain {

    public static void main(String[] args) throws InterruptedException, RejectionException, TimeoutException {

        List<String> users = new ArrayList<>();
        for (int i = 1; i <= 50; i++) {
            String name = "name" + i;
            users.add(name);
        }
        String[] names = users.subList(0, 10).toArray(new String[0]);
        test(names);

    }

    private static void test(String... clients) {
        ThreadGroup threadGroup = new ThreadGroup("clients");
        try {
            List<Thread> threads = new ArrayList<>();
            CountDownLatch latch = new CountDownLatch(clients.length);
            for (String client : clients) {
                threads.add(new Thread(threadGroup, new TestRunnable(latch, client), client));
            }
            threads.forEach(Thread::start);
        } catch (Exception e) {
            e.printStackTrace();
            threadGroup.interrupt();
        }
    }

    public static class TestRunnable implements Runnable {

        private CountDownLatch latch;
        private String login;

        public TestRunnable(CountDownLatch latch, String login) {
            this.latch = latch;
            this.login = login;
        }

        @Override
        public void run() {
            String name = Thread.currentThread().getName();
            Client client = new Client();

            System.out.format("Thread %s started\n", name);
            try {
                User user = client.login(login + "@gmail.com", login).get().getEntity();
                System.out.format("Thread %s logged in\n\n", name);
                latch.countDown();
                latch.await();

                //System.out.println("BEGIN " + name);
                for (int i = 0; i < 5; i++) {
//                    int count = testRequest(10, () -> client.getUser(1), client);
                    int count = testRequestBatched(10, 10, () -> client.getUser(1), client);

//                    int count = testRequest(10, () -> client.echo("hello"), client);
//                    int count = testRequestBatched(10, 10, () -> client.echo("hello"), client);
                    System.out.format("(%d-%s) Total: %d\n", i + 1, name, count);
                }
                //System.out.println("END " + name);

            } catch (Exception e) {
                System.out.format("ERROR in Thread %s : %s\n", name, e);
            } finally {
                client.logout();
            }
        }

        private int testRequest(int seconds, InvokableRequest request, Client client)
                throws InterruptedException, RejectionException, TimeoutException {
            client.setAutoflush(true);
            int count = 0;
            long end = System.currentTimeMillis() + seconds * 1000;
            while (System.currentTimeMillis() < end) {
                request.invoke().get();
                ++count;
            }
            return count;

        }

        private int testRequestBatched(int batch, int iterationPeriod, InvokableRequest request, Client client) {
            client.setAutoflush(false);
            int count = 0;
            long end = System.currentTimeMillis() + iterationPeriod;
            while (System.currentTimeMillis() < end) {

                CountDownLatch downLatch = new CountDownLatch(batch);
                for (int k = 0; k < batch; k++) {
                    ResponseFuture future = request.invoke();
                    future.addListener(response -> downLatch.countDown());
                }
                client.flush();
                try {
                    downLatch.await(iterationPeriod, TimeUnit.MILLISECONDS);
                } catch (InterruptedException ignored) {
                }
                count += batch - downLatch.getCount();
            }
            return count;
        }

        private interface InvokableRequest {

            ResponseFuture invoke();
        }
    }
}
