package client.codec;

public class DecodingException extends RuntimeException {

    public DecodingException() {}

    public DecodingException(String message) {
        super(message);
    }

    public DecodingException(Throwable cause) {
        super(cause);
    }
}
