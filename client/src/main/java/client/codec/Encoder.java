package client.codec;

import client.net.packet.Packet;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.MessageToByteEncoder;
import lombok.extern.log4j.Log4j2;

@Log4j2
public class Encoder extends MessageToByteEncoder<Packet> {

    private static ObjectMapper mapper = new ObjectMapper();

    /*
     * Envelope format:
     *  ____________________________HEADER(15b)_________________________   _BODY(..)__
     * /                                                                \ /           \
     * | id(8b) | version(1b) | type(1b) | code(1b) | payload_length(4b) | payload(..) |
     */
    @Override
    protected void encode(ChannelHandlerContext context, Packet packet, ByteBuf buffer) throws Exception {
        byte[] payload = mapper.writeValueAsString(packet).getBytes();
        log.info("SENT: " + new String(payload));

        buffer.writeLong(packet.getId());
        buffer.writeByte(packet.getVersion());
        buffer.writeByte(packet.getType());
        buffer.writeByte(packet.getCode());
        buffer.writeInt(payload.length);
        buffer.writeBytes(payload);

        context.flush();
    }
}
