package client.codec;

import client.net.packet.Packet;

public interface PacketDecoder<T extends Packet> {

    T decodePacket(Envelope envelope) throws DecodingException;
}
