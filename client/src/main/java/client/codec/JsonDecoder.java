package client.codec;

import client.net.packet.Packet;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.log4j.Log4j2;

import java.io.StringReader;
import java.nio.charset.StandardCharsets;

@Log4j2
public class JsonDecoder implements PacketDecoder<Packet> {

    private static ObjectMapper mapper = new ObjectMapper();

    public Packet decodePacket(Envelope envelope)
            throws DecodingException {
        try {
            String json = new String(envelope.getPayload(), StandardCharsets.UTF_8);
            log.info("RECEIVED JSON: " + (json.length() > 2000 ? "<...>" : json));
            return mapper.readValue(new StringReader(json), envelope.getPacketClass());
        } catch (Exception e) {
            throw new DecodingException(e);
        }
    }
}