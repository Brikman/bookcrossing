package client.codec;

import client.net.dto.*;
import client.net.packet.response.EntityListResponse;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.ObjectCodec;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;

import java.io.IOException;
import java.util.*;

public class EntityListResponseDeserializer extends JsonDeserializer<EntityListResponse> {

    private static Set<Class<? extends Entity>> entities = new HashSet<>(
            Arrays.asList(User.class, Book.class, Transfer.class));

    @Override
        public EntityListResponse deserialize(JsonParser parser, DeserializationContext context)
            throws IOException {
        ObjectCodec codec = parser.getCodec();
        JsonNode root = codec.readTree(parser);
        Iterator<JsonNode> iterator = root.get("entities").iterator();
        List<Entity> entityList = new ArrayList<>();
        String entityType = root.get("entityType").textValue();
        if (entityType != null) {
            Optional<Class<? extends Entity>> entityClass = entities.stream()
                    .filter(s -> s.getSimpleName().equalsIgnoreCase(entityType))
                    .findFirst();
            if (entityClass.isPresent()) {
                while (iterator.hasNext()) {
                    JsonNode entityNode = iterator.next();
                    entityList.add(codec.treeToValue(entityNode, entityClass.get()));
                }
            } else {
                throw new IOException("cannot determine entity for name: " + entityType);
            }
        }

        long requestId = root.get("id").longValue();
        byte version = (byte) root.get("version").intValue();
        return new EntityListResponse(entityList, requestId, version);
    }
}