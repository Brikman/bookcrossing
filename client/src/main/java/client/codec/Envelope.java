package client.codec;

import client.net.packet.Packet;
import lombok.Data;

@Data
public class Envelope {
    private long id;
    private byte version;
    private byte type;
    private byte code;
    private byte[] payload;
    private Class<? extends Packet> packetClass;
}
