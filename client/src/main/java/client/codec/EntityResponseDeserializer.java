package client.codec;

import client.net.dto.*;
import client.net.packet.response.EntityResponse;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.ObjectCodec;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;

import java.io.IOException;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public class EntityResponseDeserializer extends JsonDeserializer<EntityResponse> {

    private static Set<Class<? extends Entity>> entities = new HashSet<>(
            Arrays.asList(User.class, Book.class, Transfer.class));

    @Override
    public EntityResponse deserialize(JsonParser parser, DeserializationContext context) throws IOException {
        ObjectCodec codec = parser.getCodec();
        JsonNode root = codec.readTree(parser);
        long requestId = root.get("id").longValue();
        byte version = (byte) root.get("version").intValue();
        String entityType = root.get("entityType").textValue();
        Class<? extends Entity> entityClass = EntityRegistry.getEntity(entityType);
        if (entityClass != null) {
            Entity entity = codec.treeToValue(root.get("entity"), entityClass);
            return new EntityResponse(entity, requestId, version);
        }
        throw new IOException("cannot determine entity for name: " + entityType);
    }
}
