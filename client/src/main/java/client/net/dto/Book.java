package client.net.dto;

import lombok.Data;

import java.util.Collections;
import java.util.List;

@Data
public class Book implements Entity {

    private long id;
    private String title;
    private String author;
    private String description;
    private Picture picture;
    private float rating;
}
