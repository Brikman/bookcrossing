package client.net.dto;

import lombok.Data;

@Data
public class User implements Entity {

    private long id;
    private String name;
    private String email;
    private String phone;
    private Address address;
    private String about;
    private Picture picture;
}
