package client.net.dto;

import lombok.Data;

@Data
public class Picture implements Entity {

    private long id;
    private byte[] bytes;

    public Picture(byte[] bytes) {
        this.bytes = bytes;
    }
}
