package client.net.dto;

import lombok.Data;

import java.time.Instant;

@Data
public class Transfer implements Entity {

    private long id;
    private User sender;
    private User recipient;
    private Book book;
    private Instant sent;
    private String message;
    private String trackNumber;
}
