package client.net.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

@Data
public class Address implements Entity {

    private String countryCode;
    private String countryName;
    private String cityCode;
    private String cityName;
    private String street;
    private String house;
    private String apartment;
    private String postalCode;

    @JsonIgnore
    @Override
    public long getId() {
        return -1;
    }
}
