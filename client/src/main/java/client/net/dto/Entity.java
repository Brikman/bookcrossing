package client.net.dto;

import java.io.Serializable;

public interface Entity extends Serializable {

    long getId();
}
