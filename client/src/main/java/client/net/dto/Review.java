package client.net.dto;

import lombok.Data;

@Data
public class Review implements Entity {

    private long id;
    private User user;
    private Book book;
    private String comment;
    private int rating;

    @Override
    public long getId() {
        return 0;
    }
}
