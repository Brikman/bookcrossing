package client.net.packet.request;

import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@Data
public class RecipientListRequest extends Request {

    public static final int LIST_NEW = 0;
    public static final int LIST_PENDING = 1;

    private int option;

    private RecipientListRequest(int option) {
        this.option = option;
    }

    public static RecipientListRequest forNewList() {
        return new RecipientListRequest(LIST_NEW);
    }

    public static RecipientListRequest forPendingList() {
        return new RecipientListRequest(LIST_PENDING);
    }
}
