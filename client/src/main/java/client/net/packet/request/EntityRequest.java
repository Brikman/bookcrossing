package client.net.packet.request;

import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@Data
public class EntityRequest extends Request {

    public static final String USER = "user";
    public static final String BOOK = "book";

    private final long entityId;
    private final String entityName;

    public EntityRequest(long entityId, String entityName) {
        this.entityId = entityId;
        this.entityName = entityName;
    }
}
