package client.net.packet.request;

import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@Data
public class BookListRequest extends Request {

    private String condition;
    private Object value;

    private BookListRequest(String condition, Object value) {
        this.condition = condition;
        this.value = value;
    }

    public static BookListRequest history() {
        return new BookListRequest("history", null);
    }

    public static BookListRequest byUser(long userId) {
        return new BookListRequest("user", userId);
    }

    public static BookListRequest byTitle(String title) {
        return new BookListRequest("title", title);
    }
}
