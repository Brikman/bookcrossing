package client.net.packet.request;

import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@Data
public class EchoRequest extends Request {

    private String message;

    public EchoRequest(String message) {
        this.message = message;
    }
}
