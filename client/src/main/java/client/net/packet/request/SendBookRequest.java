package client.net.packet.request;

import client.net.dto.Book;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@Data
public class SendBookRequest extends Request {

    private long recipientId;
    private Book book;
    private SendOption sendOption;
    private String message;
    private String trackNumber;

    @JsonCreator
    public SendBookRequest(@JsonProperty("book") Book book,
                           @JsonProperty("recipientId") long recipientId,
                           @JsonProperty("sendOption") SendOption option,
                           @JsonProperty("message") String message,
                           @JsonProperty("trackNumber") String trackNumber) {
        this.recipientId = recipientId;
        this.book = book;
        this.message = message;
        this.trackNumber = trackNumber;
        this.sendOption = option;
    }
}
