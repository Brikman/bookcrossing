package client.net.packet.request;

public enum SendOption {
    SEND_NEW, FORWARD
}
