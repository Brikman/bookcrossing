package client.net.packet.request;

import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@Data
public class TransferListRequest extends Request {

    private TransferListOption option;

    public TransferListRequest(TransferListOption option) {
        this.option = option;
    }
}
