package client.net.packet.request;

import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@Data
public class ConfirmReceptionRequest extends Request {

    private long transferId;
    private String message;

    public ConfirmReceptionRequest(long transferId, String message) {
        this.transferId = transferId;
        this.message = message;
    }
}
