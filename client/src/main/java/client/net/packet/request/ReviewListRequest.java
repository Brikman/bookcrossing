package client.net.packet.request;

import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@Data
public class ReviewListRequest extends Request {

    private long bookId;

    public ReviewListRequest(long bookId) {
        this.bookId = bookId;
    }
}
