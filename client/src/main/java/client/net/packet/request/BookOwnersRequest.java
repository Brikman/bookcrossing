package client.net.packet.request;

import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@Data
public class BookOwnersRequest extends Request {

    private long bookId;

    public BookOwnersRequest(long bookId) {
        this.bookId = bookId;
    }
}
