package client.net.packet.request;

public enum TransferListOption {
    OUTBOUND, INCOMING
}
