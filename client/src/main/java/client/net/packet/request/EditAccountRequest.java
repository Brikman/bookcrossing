package client.net.packet.request;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Map;

@EqualsAndHashCode(callSuper = true)
@Data
public class EditAccountRequest extends Request {

    static final String NAME = "name";
    static final String EMAIL = "email";
    static final String PHONE = "phone";
    static final String ADDRESS = "address";
    static final String ABOUT = "about";
    static final String PICTURE = "picture";

    private Map<String, Object> fields;

    EditAccountRequest(Map<String, Object> fields) {
        this.fields = fields;
    }
}
