package client.net.packet.request;

import client.net.dto.Address;
import client.net.dto.Picture;

import java.util.HashMap;
import java.util.Map;

import static client.net.packet.request.EditAccountRequest.*;

public class EditAccountRequestBuilder {

    private Map<String, Object> fields = new HashMap<>();

    public EditAccountRequestBuilder setName(String name) {
        fields.put(NAME, trim(name));
        return this;
    }

    public EditAccountRequestBuilder setEmail(String email) {
        fields.put(EMAIL, trim(email));
        return this;
    }

    public EditAccountRequestBuilder setPhone(String phone) {
        fields.put(PHONE, trim(phone));
        return this;
    }

    public EditAccountRequestBuilder setAddress(Address address) {
        fields.put(ADDRESS, address);
        return this;
    }

    public EditAccountRequestBuilder setAbout(String about) {
        fields.put(ABOUT, trim(about));
        return this;
    }

    public EditAccountRequestBuilder setPicture(Picture picture) {
        fields.put(PICTURE, picture);
        return this;
    }

    public EditAccountRequest build() {
        return new EditAccountRequest(fields);
    }

    private String trim(String str) {
        return str != null ? str.trim() : str;
    }
}
