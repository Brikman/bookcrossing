package client.net.packet.request;

import client.net.dto.*;
import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@Data
public class PictureRequest extends Request {

    private String entityName;
    private long entityId;

    public PictureRequest(Entity entity) {
        entityId = entity.getId();
        entityName = EntityRegistry.getKey(entity);
    }
}
