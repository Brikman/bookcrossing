package client.net.packet.request;

import client.net.packet.Packet;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.concurrent.atomic.AtomicLong;

public abstract class Request extends Packet {

    @JsonIgnore
    private static AtomicLong counter = new AtomicLong(0);

    protected Request() {
        super(counter.incrementAndGet());
    }
}
