package client.net.packet.response;


import client.net.dto.Picture;
import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@Data
public class PictureResponse extends Response {

    private Picture picture;

    public PictureResponse(Picture picture, long requestId, byte version) {
        super(requestId, version);
        this.picture = picture;
    }
}
