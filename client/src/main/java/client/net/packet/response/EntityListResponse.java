package client.net.packet.response;

import client.codec.EntityListResponseDeserializer;
import client.net.dto.Entity;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@EqualsAndHashCode(callSuper = true)
@Data
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonDeserialize(using = EntityListResponseDeserializer.class)
public class EntityListResponse<T extends Entity> extends Response {

    private List<T> entities;

    @JsonCreator
    public EntityListResponse(@JsonProperty("entities") Collection<T> entities,
                              @JsonProperty("requestId") long requestId,
                              @JsonProperty("version") byte version) {
        super(requestId, version);
        this.entities = new ArrayList<>(entities);
    }
}
