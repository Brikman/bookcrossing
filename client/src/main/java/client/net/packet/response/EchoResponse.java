package client.net.packet.response;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@Data
public class EchoResponse extends Response {

    private String message;

    @JsonCreator
    public EchoResponse(@JsonProperty("message") String message,
                        @JsonProperty("requestId") long requestId,
                        @JsonProperty("version") byte version) {
        super(requestId, version);
        this.message = message;
    }
}
