package client.net.packet.response;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@Data
public class StatusResponse extends Response {

    private boolean status;
    private String  message;

    @JsonCreator
    public StatusResponse(@JsonProperty("status") boolean status,
                          @JsonProperty("message") String message,
                          @JsonProperty("requestId") long requestId,
                          @JsonProperty("version") byte version) {
        super(requestId, version);
        this.status = status;
        this.message = message;
    }

    @Override
    public String toString() {
        return "StatusResponse{" +
                "status=" + status +
                ", message='" + message + '\'' +
                '}';
    }
}
