package client.net.packet.response;


import client.net.dto.User;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;
import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class RecipientListResponse extends Response {

    private List<User> recipients;
    private int availableRecipients;

    @JsonCreator
    public RecipientListResponse(@JsonProperty("recipients") List<User> recipients,
                                 @JsonProperty("availableRecipients") int availableRecipients,
                                 @JsonProperty("requestId") long requestId,
                                 @JsonProperty("version") byte version) {
        super(requestId, version);
        this.recipients = recipients != null ? recipients : new ArrayList<>();
        this.availableRecipients = availableRecipients;
    }

    public List<User> getRecipients() {
        return recipients;
    }

    public int getAvailableRecipients() {
        return availableRecipients;
    }

    @JsonIgnore
    public int getPendingCount() {
        return recipients.size();
    }
}