package client.net.packet.response;

import client.net.dto.Book;
import client.net.dto.Review;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

@EqualsAndHashCode(callSuper = true)
@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class ReviewListResponse extends Response {

    private Book book;
    private List<Review> reviews;

    @JsonCreator
    public ReviewListResponse(@JsonProperty("book") Book book,
                              @JsonProperty("entities") List<Review> reviews,
                              @JsonProperty("requestId") long requestId,
                              @JsonProperty("version") byte version) {
        super(requestId, version);
        this.book = book;
        this.reviews = reviews;
    }
}
