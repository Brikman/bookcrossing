package client.net.packet.response;

import client.net.packet.Packet;
import client.net.packet.request.Request;

public abstract class Response extends Packet {

    protected Response(long requestId, byte version) {
        super(requestId, version);
    }
}
