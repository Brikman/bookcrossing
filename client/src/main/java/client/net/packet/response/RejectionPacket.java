package client.net.packet.response;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@Data
public class RejectionPacket extends Response {

    private final String message;

    @JsonCreator
    public RejectionPacket(@JsonProperty("message") String message,
                           @JsonProperty("requestId") long requestId,
                           @JsonProperty("version") byte version) {
        super(requestId, version);
        this.message = message;
    }
}
