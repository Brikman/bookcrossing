package client.net.packet.response;

import client.codec.EntityResponseDeserializer;
import client.net.dto.Entity;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@Data
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonDeserialize(using = EntityResponseDeserializer.class)
public class EntityResponse<T extends Entity> extends Response {

    private final T entity;

    @JsonCreator
    public EntityResponse(@JsonProperty("entity") T entity,
                          @JsonProperty("requestId") long requestId,
                          @JsonProperty("version") byte version) {
        super(requestId, version);
        this.entity = entity;
    }
}
