package client.net.packet;

import client.net.packet.request.*;
import client.net.packet.response.*;

import java.util.HashMap;
import java.util.Map;

public class PacketRegistry<T extends Packet> {

    private static final Map<Integer, Class<? extends Request>> requestMap
            = new HashMap<Integer, Class<? extends Request>>(){{
        put(0, EchoRequest.class);
        put(1, AuthRequest.class);
        put(2, BookListRequest.class);
        put(3, ConfirmReceptionRequest.class);
        put(4, EditAccountRequest.class);
        put(5, EntityRequest.class);
        put(6, RecipientListRequest.class);
        put(7, RegistrationRequest.class);
        put(8, SendBookRequest.class);
        put(9, TransferListRequest.class);
        put(10, PictureRequest.class);
        put(13, BookOwnersRequest.class);
        put(14, LeaveReviewRequest.class);
        put(15, ReviewListRequest.class);
    }};

    private static final Map<Integer, Class<? extends Response>> responseMap
            = new HashMap<Integer, Class<? extends Response>>(){{
        put(0, EchoResponse.class);
        put(1, StatusResponse.class);
        put(2, RejectionPacket.class);
        put(3, EntityResponse.class);
        put(4, EntityListResponse.class);
        put(5, PictureResponse.class);
        put(6, RecipientListResponse.class);
        put(7, ReviewListResponse.class);
    }};

    private final Map<Integer, Class<T>> registryMap;

    private PacketRegistry(Map<Integer, Class<T>> registry) {
        registryMap = registry;
    }

    private static final PacketRegistry<Request> REQUEST_REG = new PacketRegistry(requestMap);
    private static final PacketRegistry<Response> RESPONSE_REG = new PacketRegistry(responseMap);

    public static PacketRegistry forType(byte type) {
        if (type == 1) {
            return REQUEST_REG;
        } else if (type == 2) {
            return RESPONSE_REG;
        }
        throw new IllegalArgumentException("Invalid packet type: " + type);
    }

    public Class<T> getPacket(byte code) {
        Class<T> packetClass = registryMap.get((int) code);
        if (packetClass != null)
            return packetClass;
        throw new IllegalArgumentException("Invalid packet code: " + code);
    }

    public static byte getPacketCode(Packet packet) {
        if (packet instanceof Request) {
            return getPacketCode(requestMap, packet);
        } else {
            return getPacketCode(responseMap, packet);
        }
    }

    private static byte getPacketCode(Map<Integer, ? extends Class<? extends Packet>> packetMap,
                                      Packet packet) {
        return packetMap.entrySet().stream()
                .filter(entry -> entry.getValue() == packet.getClass())
                .findFirst()
                .map(Map.Entry::getKey)
                .map(Integer::byteValue)
                .orElse(null);
    }
}
