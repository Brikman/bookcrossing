package client.net.packet;

import client.net.packet.request.Request;
import client.net.packet.response.Response;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.io.Serializable;

@Data
public abstract class Packet implements Serializable {

    private long id;
    private byte code;
    private byte version;

    protected Packet(long id) {
        this(id, (byte) 1);
    }

    protected Packet(long id, byte version) {
        this.id = id;
        this.version = version;
        this.code = PacketRegistry.getPacketCode(this);
    }

    @JsonIgnore
    public byte getType() {
        if (this instanceof Request)
            return 1;
        if (this instanceof Response)
            return 2;
        return 3;
    }
}
